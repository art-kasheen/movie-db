# A movie app created with React and The Movie Database API

## Before start create an api key [here](https://www.themoviedb.org/documentation/api) and then create an `.env` file on the root of the project and put created api key

```
// .env
'REACT_APP_API_KEY'=api_key
```

## Installing

```
yarn install
yarn start
```

Developed by **kashyr**
