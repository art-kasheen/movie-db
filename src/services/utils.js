import { Map } from 'immutable'

export function toMap(values, Schema) {
  return values.reduce((acc, value) => {
    return acc.set(value.id, new Schema(value))
  }, new Map({}))
}
