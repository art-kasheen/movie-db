export const getFormatedTime = (seconds) =>
  `${Math.floor(seconds / 60)}h ${Math.round(
    (seconds / 60 - Math.floor(seconds / 60)) * 60
  )} min`
