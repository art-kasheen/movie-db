import axios from 'axios'

export default axios.create({
  params: {
    api_key: process.env.REACT_APP_API_KEY
  },
  baseURL: 'https://api.themoviedb.org/3/'
})
