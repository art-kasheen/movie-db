export const person = {
  birthday: '1974-11-11',
  known_for_department: 'Acting',
  deathday: null,
  id: 6193,
  name: 'Leonardo DiCaprio',
  also_known_as: [
    'Леонардо ДиКаприо',
    'ليوناردو دي كابريو',
    '레오나르도 디카프리오',
    'レオナルド・ディカプリオ',
    'ลีโอนาร์โด ดิแคพรีโอ',
    '莱昂纳多·迪卡普里奥',
    'Leo DiCaprio',
    'Leonardo Retardo',
    'Leo',
    'Leonardo Wilhelm DiCaprio',
    'Λεονάρντο Ντι Κάπριο',
    'Ντι Κάπριο'
  ],
  gender: 2,
  biography:
    "Leonardo Wilhelm DiCaprio (born November 11, 1974) is an American actor, film producer, and environmental activist.\n\nHe began his film career by starring as Josh in Critters 3 before starring in the film adaptation of the memoir This Boy's Life (1993) alongside Robert De Niro. DiCaprio was praised for his supporting role in the drama What's Eating Gilbert Grape (1993), and gained public recognition with leading roles in the drama The Basketball Diaries (1995) and the romantic drama Romeo + Juliet (1996), before achieving international fame with James Cameron's epic romance Titanic (1997) He has been nominated for six Academy Awards—five for acting and one for producing—and in 2016, he won the Academy Award for Best Actor for The Revenant.\n\nDiCaprio is the founder of his own production company, named Appian Way Productions. He is also a committed environmentalist.",
  popularity: 10.031,
  place_of_birth: 'Los Angeles, California, USA',
  profile_path: '/aLUFp0zWpLVyIOgY0scIpuuKZLE.jpg',
  adult: false,
  imdb_id: 'nm0000138',
  homepage: 'http://leonardodicaprio.com'
}
