export const movies = {
    "results": [
        {
            "vote_count": 1307,
            "id": 420818,
            "video": false,
            "vote_average": 7.2,
            "title": "The Lion King",
            "popularity": 383.796,
            "poster_path": "/dzBtMocZuJbjLOXvrl4zGYigDzh.jpg",
            "original_language": "en",
            "original_title": "The Lion King",
            "genre_ids": [
                12,
                16,
                10751,
                18,
                28
            ],
            "backdrop_path": "/1TUg5pO1VZ4B0Q1amk3OlXvlpXV.jpg",
            "adult": false,
            "overview": "Simba idolises his father, King Mufasa, and takes to heart his own royal destiny. But not everyone in the kingdom celebrates the new cub's arrival. Scar, Mufasa's brother—and former heir to the throne—has plans of his own. The battle for Pride Rock is ravaged with betrayal, tragedy and drama, ultimately resulting in Simba's exile. With help from a curious pair of newfound friends, Simba will have to figure out how to grow up and take back what is rightfully his.",
            "release_date": "2019-07-12"
        },
        {
            "vote_count": 105,
            "id": 384018,
            "video": false,
            "vote_average": 6.8,
            "title": "Fast & Furious Presents: Hobbs & Shaw",
            "popularity": 299.856,
            "poster_path": "/keym7MPn1icW1wWfzMnW3HeuzWU.jpg",
            "original_language": "en",
            "original_title": "Fast & Furious Presents: Hobbs & Shaw",
            "genre_ids": [
                28
            ],
            "backdrop_path": "/hpgda6P9GutvdkDX5MUJ92QG9aj.jpg",
            "adult": false,
            "overview": "A spinoff of The Fate of the Furious, focusing on Johnson's US Diplomatic Security Agent Luke Hobbs forming an unlikely alliance with Statham's Deckard Shaw.",
            "release_date": "2019-08-01"
        },
        {
            "vote_count": 30,
            "id": 506574,
            "video": false,
            "vote_average": 9.6,
            "title": "Descendants 3",
            "popularity": 161.524,
            "poster_path": "/dwQH7sb9BWxpQ4YpX2RUmrKe2PP.jpg",
            "original_language": "en",
            "original_title": "Descendants 3",
            "genre_ids": [
                12,
                10751,
                10402
            ],
            "backdrop_path": "/clswpC7f3hSvJlhRnymCFY0Eqm9.jpg",
            "adult": false,
            "overview": "The teenagers of Disney's most infamous villains return to the Isle of the Lost to recruit a new batch of villainous offspring to join them at Auradon Prep.",
            "release_date": "2019-08-09"
        },
        {
            "vote_count": 159,
            "id": 466272,
            "video": false,
            "vote_average": 7.6,
            "title": "Once Upon a Time in Hollywood",
            "popularity": 80.734,
            "poster_path": "/8j58iEBw9pOXFD2L0nt0ZXeHviB.jpg",
            "original_language": "en",
            "original_title": "Once Upon a Time in Hollywood",
            "genre_ids": [
                18,
                35,
                28,
                80,
                37
            ],
            "backdrop_path": "/b82nVVhYNRgtsTFV1jkdDwe6LJZ.jpg",
            "adult": false,
            "overview": "A faded television actor and his stunt double strive to achieve fame and success in the film industry during the final years of Hollywood's Golden Age in 1969 Los Angeles.",
            "release_date": "2019-07-25"
        },
        {
            "vote_count": 1716,
            "id": 301528,
            "video": false,
            "vote_average": 7.7,
            "title": "Toy Story 4",
            "popularity": 79.36,
            "poster_path": "/w9kR8qbmQ01HwnvK4alvnQ2ca0L.jpg",
            "original_language": "en",
            "original_title": "Toy Story 4",
            "genre_ids": [
                12,
                16,
                35,
                10751
            ],
            "backdrop_path": "/m67smI1IIMmYzCl9axvKNULVKLr.jpg",
            "adult": false,
            "overview": "Woody has always been confident about his place in the world and that his priority is taking care of his kid, whether that's Andy or Bonnie. But when Bonnie adds a reluctant new toy called \"Forky\" to her room, a road trip adventure alongside old and new friends will show Woody how big the world can be for a toy.",
            "release_date": "2019-06-19"
        },
        {
            "vote_count": 172,
            "id": 511987,
            "video": false,
            "vote_average": 5.9,
            "title": "Crawl",
            "popularity": 56.278,
            "poster_path": "/mKxpYRIrCZLxZjNqpocJ2RdQW8v.jpg",
            "original_language": "en",
            "original_title": "Crawl",
            "genre_ids": [
                53,
                28,
                27
            ],
            "backdrop_path": "/2cAce3oD0Hh7f5XxuXmNXa1WiuZ.jpg",
            "adult": false,
            "overview": "While struggling to save her father during a Category 5 hurricane, a young woman finds herself trapped inside a flooding house and fighting for her life against Florida’s most savage and feared predators.",
            "release_date": "2019-07-11"
        },
        {
            "vote_count": 1,
            "id": 571627,
            "video": false,
            "vote_average": 1,
            "title": "The Divine Fury",
            "popularity": 55.9,
            "poster_path": "/9Z2qT9iZYLzzsCSYu7A4SEQsKX0.jpg",
            "original_language": "ko",
            "original_title": "사자",
            "genre_ids": [
                28,
                27,
                9648
            ],
            "backdrop_path": "/4MDYpCwqSVO5RcTiZ4GEfePzDdl.jpg",
            "adult": false,
            "overview": "Yong-hu, a world champion martial artist who believes in no God, only himself, suddenly develops stigmata on his palms. Visiting a church in hopes of healing the stigmata, he ends up saving Father AHN, who falls into danger while performing an exorcism. In this way he learns of the power of the stigmata. As time passes Yong-hu accepts his new calling and begins saving people who are possessed by demons, but then he must prepare for a final confrontation with the evil Jisin, who ensnares people with the power of the devil.",
            "release_date": "2019-07-31"
        },
        {
            "vote_count": 828,
            "id": 456740,
            "video": false,
            "vote_average": 5,
            "title": "Hellboy",
            "popularity": 53.937,
            "poster_path": "/bk8LyaMqUtaQ9hUShuvFznQYQKR.jpg",
            "original_language": "en",
            "original_title": "Hellboy",
            "genre_ids": [
                28,
                12,
                14,
                27,
                878
            ],
            "backdrop_path": "/5BkSkNtfrnTuKOtTaZhl8avn4wU.jpg",
            "adult": false,
            "overview": "Hellboy comes to England, where he must defeat Nimue, Merlin's consort and the Blood Queen. But their battle will bring about the end of the world, a fate he desperately tries to turn away.",
            "release_date": "2019-04-10"
        },
        {
            "vote_count": 7,
            "id": 454640,
            "video": false,
            "vote_average": 5.6,
            "title": "The Angry Birds Movie 2",
            "popularity": 49.898,
            "poster_path": "/hdYTrfRKrzdbZ3DE72YxmeB0RNg.jpg",
            "original_language": "en",
            "original_title": "The Angry Birds Movie 2",
            "genre_ids": [
                16,
                35,
                28,
                12,
                10751
            ],
            "backdrop_path": "/k7sE3loFwuU2mqf7FbZBeE3rjBa.jpg",
            "adult": false,
            "overview": "The flightless birds and scheming green pigs take their beef to the next level.",
            "release_date": "2019-08-02"
        },
        {
            "vote_count": 1675,
            "id": 537915,
            "video": false,
            "vote_average": 5.9,
            "title": "After",
            "popularity": 42.661,
            "poster_path": "/u3B2YKUjWABcxXZ6Nm9h10hLUbh.jpg",
            "original_language": "en",
            "original_title": "After",
            "genre_ids": [
                18,
                10749
            ],
            "backdrop_path": "/997ToEZvF2Obp9zNZbY5ELVnmrW.jpg",
            "adult": false,
            "overview": "Tessa Young is a dedicated student, dutiful daughter and loyal girlfriend to her high school sweetheart. Entering her first semester of college, Tessa's guarded world opens up when she meets Hardin Scott, a mysterious and brooding rebel who makes her question all she thought she knew about herself -- and what she wants out of life.",
            "release_date": "2019-04-11"
        },
        {
            "vote_count": 262,
            "id": 530385,
            "video": false,
            "vote_average": 7.3,
            "title": "Midsommar",
            "popularity": 36.757,
            "poster_path": "/eycO6M2s38xO1888Gq2gSrXf0A6.jpg",
            "original_language": "en",
            "original_title": "Midsommar",
            "genre_ids": [
                27,
                18
            ],
            "backdrop_path": "/8yE6wv1l570zWyh6zmkFxysw3Kc.jpg",
            "adult": false,
            "overview": "A young couple travels to Sweden to visit their friend’s rural hometown and attend its mid-summer festival. What begins as an idyllic retreat quickly descends into an increasingly violent and bizarre competition at the hands of a pagan cult.",
            "release_date": "2019-07-03"
        },
        {
            "vote_count": 279,
            "id": 412117,
            "video": false,
            "vote_average": 6.5,
            "title": "The Secret Life of Pets 2",
            "popularity": 32.484,
            "poster_path": "/q3mKnSkzp1doIsCye6ap4KIUAbu.jpg",
            "original_language": "en",
            "original_title": "The Secret Life of Pets 2",
            "genre_ids": [
                12,
                16,
                35,
                10751
            ],
            "backdrop_path": "/etaok7q2E5tV36oXe7GQzhUQ4fX.jpg",
            "adult": false,
            "overview": "Max the terrier must cope with some major life changes when his owner gets married and has a baby. When the family takes a trip to the countryside, nervous Max has numerous run-ins with canine-intolerant cows, hostile foxes and a scary turkey. Luckily for Max, he soon catches a break when he meets Rooster, a gruff farm dog who tries to cure the lovable pooch of his neuroses.",
            "release_date": "2019-05-24"
        },
        {
            "vote_count": 59,
            "id": 513045,
            "video": false,
            "vote_average": 6.7,
            "title": "Stuber",
            "popularity": 25.774,
            "poster_path": "/7RTaeiHvc9oPfvRMQGUra7qLOQh.jpg",
            "original_language": "en",
            "original_title": "Stuber",
            "genre_ids": [
                28,
                35
            ],
            "backdrop_path": "/xgfn98c2UzvFWP6MXDzytearmQ3.jpg",
            "adult": false,
            "overview": "After crashing his car, a cop who's recovering from eye surgery recruits an Uber driver to help him catch a heroin dealer. The mismatched pair soon find themselves in for a wild day of stakeouts and shootouts as they encounter the city's seedy side.",
            "release_date": "2019-07-11"
        },
        {
            "vote_count": 0,
            "id": 499701,
            "video": false,
            "vote_average": 0,
            "title": "Dora and the Lost City of Gold",
            "popularity": 24.953,
            "poster_path": "/xvYCZ740XvngXK0FNeSNVTJJJ5v.jpg",
            "original_language": "en",
            "original_title": "Dora and the Lost City of Gold",
            "genre_ids": [
                12,
                35,
                10751
            ],
            "backdrop_path": "/843PwG97xLcz7TUW8tKDNrOc2sj.jpg",
            "adult": false,
            "overview": "Dora, a girl who has spent most of her life exploring the jungle with her parents, now must navigate her most dangerous adventure yet: high school. Always the explorer, Dora quickly finds herself leading Boots (her best friend, a monkey), Diego, and a rag tag group of teens on an adventure to save her parents and solve the impossible mystery behind a lost Inca civilization.",
            "release_date": "2019-08-08"
        },
        {
            "vote_count": 781,
            "id": 504608,
            "video": false,
            "vote_average": 7.6,
            "title": "Rocketman",
            "popularity": 24.233,
            "poster_path": "/f4FF18ia7yTvHf2izNrHqBmgH8U.jpg",
            "original_language": "en",
            "original_title": "Rocketman",
            "genre_ids": [
                10402,
                18
            ],
            "backdrop_path": "/oAr5bgf49vxga9etWoQpAeRMvhp.jpg",
            "adult": false,
            "overview": "The story of Elton John's life, from his years as a prodigy at the Royal Academy of Music through his influential and enduring musical partnership with Bernie Taupin.",
            "release_date": "2019-05-22"
        },
        {
            "vote_count": 230,
            "id": 533642,
            "video": false,
            "vote_average": 5.9,
            "title": "Child's Play",
            "popularity": 23.514,
            "poster_path": "/rpS7ROczWulqfaXG2klYapULXKm.jpg",
            "original_language": "en",
            "original_title": "Child's Play",
            "genre_ids": [
                27,
                878
            ],
            "backdrop_path": "/vHse4QK31Vc3X7BKKU5GOQhYxv6.jpg",
            "adult": false,
            "overview": "Karen, a single mother, gifts her son Andy a Buddi doll for his birthday, unaware of its more sinister nature. A contemporary re-imagining of the 1988 horror classic.",
            "release_date": "2019-06-19"
        },
        {
            "vote_count": 34,
            "id": 454458,
            "video": false,
            "vote_average": 7.1,
            "title": "UglyDolls",
            "popularity": 20.595,
            "poster_path": "/p97LQ1TL95mptVL2AS8Rz1cJCQg.jpg",
            "original_language": "en",
            "original_title": "UglyDolls",
            "genre_ids": [
                16,
                35,
                10751,
                12,
                14
            ],
            "backdrop_path": "/5JkH3i0mvvCGMjTvmcHQB6g9XCR.jpg",
            "adult": false,
            "overview": "In the adorably different town of Uglyville, weirdness is celebrated, strangeness is special and beauty is embraced as more than meets the eye. After traveling to the other side of a mountain, Moxy and her UglyDoll friends discover Perfection -- a town where more conventional dolls receive training before entering the real world to find the love of a child.",
            "release_date": "2019-05-01"
        },
        {
            "vote_count": 168,
            "id": 400157,
            "video": false,
            "vote_average": 6.2,
            "title": "Wonder Park",
            "popularity": 20.172,
            "poster_path": "/8KomINZhIuJeB4oB7k7tkq8tmE.jpg",
            "original_language": "en",
            "original_title": "Wonder Park",
            "genre_ids": [
                35,
                16,
                12,
                10751,
                14
            ],
            "backdrop_path": "/nJyAdKPnW15IAvC6sLBTE0lp6Dv.jpg",
            "adult": false,
            "overview": "The story of a magnificent amusement park where the imagination of a wildly creative girl named June comes alive.",
            "release_date": "2019-03-14"
        },
        {
            "vote_count": 660,
            "id": 505948,
            "video": false,
            "vote_average": 6.6,
            "title": "I Am Mother",
            "popularity": 18.943,
            "poster_path": "/eItrj5GcjvCI3oD3bIcz1A2IL9t.jpg",
            "original_language": "en",
            "original_title": "I Am Mother",
            "genre_ids": [
                53,
                878
            ],
            "backdrop_path": "/kmPnDn9mbjD9Vzn1FTclNiSHGNa.jpg",
            "adult": false,
            "overview": "A teenage girl is raised underground by a robot \"Mother\", designed to repopulate the earth following an extinction event. But their unique bond is threatened when an inexplicable stranger arrives with alarming news.",
            "release_date": "2019-06-07"
        },
        {
            "vote_count": 0,
            "id": 423204,
            "video": false,
            "vote_average": 0,
            "title": "Angel Has Fallen",
            "popularity": 17.406,
            "poster_path": "/f8LmNeKqpysuIGUHGKjrXOk6Fuk.jpg",
            "original_language": "en",
            "original_title": "Angel Has Fallen",
            "genre_ids": [
                28,
                53,
                18
            ],
            "backdrop_path": "/qOeSTdwG9AqsENm6QsgVLfZ7N9E.jpg",
            "adult": false,
            "overview": "Secret Service Agent Mike Banning is framed for the attempted assassination of the President and must evade his own agency and the FBI as he tries to uncover the real threat.",
            "release_date": "2019-08-21"
        }
    ],
    "page": 1,
    "total_results": 237,
    "dates": {
        "maximum": "2019-08-26",
        "minimum": "2019-08-09"
    },
    "total_pages": 12
}