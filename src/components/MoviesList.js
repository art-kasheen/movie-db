import React from 'react'
import PropTypes from 'prop-types'
import MovieItem from './MovieItem'
import Pagination from './Pagination'
import styled from 'styled-components'

const MoviesList = ({ pagination, movies }) => {
  if (!movies || !movies.length) return null

  const items = movies.map((id) => (
    <li key={id} data-test="movie-list-item">
      <MovieItem id={id} />
    </li>
  ))

  return (
    <>
      <List>{items}</List>
      <Pagination config={pagination} />
    </>
  )
}

const List = styled.div`
  list-style-type: none;
  padding: 25px 0 0;
  margin: 0;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-row-gap: 30px;
  grid-column-gap: 30px;
`

MoviesList.propTypes = {
  movies: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired
}

export default MoviesList
