import React from 'react'
import PropTypes from 'prop-types'
import Pager from '@atlaskit/pagination'
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'
import styled from 'styled-components'

const Pagination = ({ config, location, history }) => {
  if (!config || config.get('pages') <= 1) return null

  const pages = Array.from(Array(config.get('pages')), (e, i) => i + 1)
  const query = queryString.parse(location.search)

  const handleChange = (ev, newPage) => {
    query.page = newPage
    history.push(`${location.pathname}?${queryString.stringify(query)}`)
  }

  return (
    <Wrapper>
      <Pager
        pages={pages}
        onChange={handleChange}
        selectedIndex={query.page ? Number(query.page - 1) : 0}
        handleChange={handleChange}
      />
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  padding-top: 45px;
`

Pagination.propTypes = {
  config: PropTypes.object.isRequired
}

export default withRouter(Pagination)
