import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import Button from '@atlaskit/button'

const ButtonLink = ({ href, children, ...props }) => {
  return href ? (
    <Button
      {...props}
      href={href}
      component={React.forwardRef(({ href = '', children, ...rest }, ref) => (
        <Link {...rest} to={href} innerRef={ref}>
          {children}
        </Link>
      ))}
    >
      {children}
    </Button>
  ) : (
    <Button {...props}>{children}</Button>
  )
}

ButtonLink.propTypes = {
  href: PropTypes.string,
  children: PropTypes.string
}

export default ButtonLink
