import React from 'react'
import TextField from '@atlaskit/textfield'

const Input = ({ input, ...rest }) => {
  return <TextField {...input} {...rest} />
}

export default Input
