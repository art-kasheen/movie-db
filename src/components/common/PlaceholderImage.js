import React from 'react'
import PropTypes from 'prop-types'
import placeholder from '../../assets/img/placeholder.png'

const PlaceholderImage = ({ baseUrl, path, alt, ...props }) => {
  const imagePath = baseUrl && path ? `${baseUrl}w342${path}` : placeholder

  return <img src={imagePath} alt={alt} {...props} />
}

PlaceholderImage.propTypes = {
  baseUrl: PropTypes.string.isRequired,
  path: PropTypes.string,
  alt: PropTypes.string.isRequired
}

export default PlaceholderImage
