import React from 'react'
import { connect } from 'react-redux'
import { isAuthenticated } from '../../selectors'
import { Route, Redirect } from 'react-router-dom'

const ProtectedRoute = ({ isAuthenticated, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to="/auth/signin" />
        )
      }
    />
  )
}

export default connect((state) => ({
  isAuthenticated: isAuthenticated(state)
}))(ProtectedRoute)
