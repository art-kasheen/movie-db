import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { gridSize as gridSizeFn } from '@atlaskit/theme'
import GlobalNavigation from './Navigation/GlobalNavigation'
import ContainerNavigation from './Navigation/ContainerNavigation'
import { LayoutManager, NavigationProvider } from '@atlaskit/navigation-next'
import { Switch, Route, Redirect } from 'react-router'
import { Wrapper } from '../styles/Layout'
import SearchPage from './routes/SearchPage'
import MoviePage from './routes/MoviePage'
import PersonPage from './routes/PersonPage'
import DiscoverPage from './routes/DiscoverPage'
import GenrePage from './routes/GenrePage'
import AuthPage from './routes/AuthPage'
import DashboardPage from './routes/DashboardPage'
import ProtectedRoute from './common/ProtectedRoute'
import Message from './Message'
import { connect } from 'react-redux'
import { appInit } from '../actions'
import Loader from './Loader'

const gridSize = gridSizeFn()

const Root = ({ appInit, initialized }) => {
  useEffect(() => {
    appInit()
    // eslint-disable-next-line
  }, [])

  if (!initialized) return <Loader />

  return (
    <NavigationProvider initialUIController={{ isResizeDisabled: true }}>
      <LayoutManager
        globalNavigation={GlobalNavigation}
        productNavigation={() => null}
        containerNavigation={ContainerNavigation}
      >
        <Wrapper style={{ padding: `${gridSize * 4}px ${gridSize * 5}px` }}>
          <Message />
          <Switch>
            <Redirect exact from="/" to="/discover/popular" />
            <Route path="/discover/:type" exact component={DiscoverPage} />
            <Route path="/search/:query" exact component={SearchPage} />
            <Route path="/movies/:id" exact component={MoviePage} />
            <Route path="/person/:id" exact component={PersonPage} />
            <Route path="/genre/:id" exact component={GenrePage} />
            <Route path="/auth" component={AuthPage} />
            <ProtectedRoute path="/dashboard" component={DashboardPage} />
            <Route render={() => <p>Not found</p>} />
          </Switch>
        </Wrapper>
      </LayoutManager>
    </NavigationProvider>
  )
}

Root.propTypes = {
  initialized: PropTypes.bool.isRequired,
  appInit: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    initialized: state.global.initialized
  }),
  { appInit }
)(Root)
