import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fecthCast } from '../actions'
import { getCast } from '../selectors'
import styled from 'styled-components'
import CastItem from './CastItem'

const Cast = ({ movieId, cast, baseUrl, fecthCast }) => {
  useEffect(() => {
    fecthCast(movieId)
    // eslint-disable-next-line
  }, [movieId])

  if (!cast || !cast.length) return null

  const items = cast.map((person) => (
    <ListItem key={person.id}>
      <CastItem person={person} baseUrl={baseUrl} />
    </ListItem>
  ))

  return (
    <Wrapper>
      <List>{items}</List>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  padding-bottom: 25px;
`

const List = styled.ul`
  display: flex;
  padding: 0;
  margin: 0;
  list-style-type: none;
`

const ListItem = styled.li`
  flex: 0 0 16.66666%;
  padding: 0 5px;
  box-sizing: border-box;
`

Cast.propTypes = {
  fecthCast: PropTypes.func.isRequired,
  cast: PropTypes.array.isRequired,
  movieId: PropTypes.number.isRequired,
  baseUrl: PropTypes.string.isRequired
}

export default connect(
  (state, props) => ({
    cast: getCast(state, props.movieId),
    baseUrl: state.global.baseUrl
  }),
  { fecthCast }
)(Cast)
