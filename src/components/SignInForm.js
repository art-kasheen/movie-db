import React from 'react'
import Input from './common/FormInput'
import Button, { ButtonGroup } from '@atlaskit/button'
import { Field as FormField, FormFooter } from '@atlaskit/form'
import { Field, reduxForm } from 'redux-form'
import styled from 'styled-components'

const SignInForm = ({ loading, handleSubmit, reset }) => {
  return (
    <Wrapper>
      <h1>Sign In</h1>
      <form onSubmit={handleSubmit}>
        <FormField name="username" label="User name" isRequired>
          {({ fieldProps }) => <Field {...fieldProps} component={Input} />}
        </FormField>
        <FormField name="password" label="Password" isRequired>
          {({ fieldProps }) => (
            <Field type="password" {...fieldProps} component={Input} />
          )}
        </FormField>
        <FormFooter>
          <ButtonGroup>
            <Button appearance="subtle" type="button" onClick={reset}>
              Cancel
            </Button>
            <Button type="submit" appearance="primary" isLoading={loading}>
              Sign in
            </Button>
          </ButtonGroup>
        </FormFooter>
      </form>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  width: 400px;
  max-width: 100%;
  margin: 0 auto;
  flex-direction: column;
`

export default reduxForm({
  form: 'signin'
})(SignInForm)
