import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import queryString from 'query-string'
import { connect } from 'react-redux'
import { fetchWatchList } from '../../actions'
import { getMovies } from '../../selectors'
import RatedMovieList from '../RatedMovieList'
import Loader from '../Loader'

const Watchlist = ({
  match,
  location,
  movies,
  loading,
  loaded,
  pagination,
  fetchWatchList
}) => {
  const { list } = match.params
  const { page } = queryString.parse(location.search)

  useEffect(() => {
    fetchWatchList({
      type: list,
      page: page
    })
    // eslint-disable-next-line
  }, [list, page])

  if (!loaded || loading) return <Loader />

  return <RatedMovieList movies={movies} pagination={pagination} />
}

Watchlist.propTypes = {
  movies: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  loaded: PropTypes.bool.isRequired,
  pagination: PropTypes.object,
  fetchWatchList: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    movies: getMovies(state),
    loading: state.movies.loading,
    loaded: state.movies.loaded,
    pagination: state.movies.pagination
  }),
  { fetchWatchList }
)(Watchlist)
