import React from 'react'
import { shallow } from 'enzyme'
import MoviesList from './MoviesList'
import { movies } from '../mocks/movies'

describe('Movies List', () => {
  test('should render movies list', () => {
    const container = shallow(
      <MoviesList movies={movies.results} pagination={{}} />
    )

    const items = container.find('[data-test="movie-list-item"]')

    expect(items.length).toEqual(movies.results.length)
  })
})
