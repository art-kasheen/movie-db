import React from 'react'
import PropTypes from 'prop-types'
import RatedMovieItem from './RatedMovieItem'
import Loader from './Loader'
import Pagination from './Pagination'
import styled from 'styled-components'

const RatedMovieList = ({ pagination, movies, loading }) => {
  if (loading) return <Loader />

  const items = movies.map((id) => (
    <li key={id}>
      <RatedMovieItem id={id} />
    </li>
  ))

  return (
    <>
      <List>{items}</List>
      <Pagination config={pagination} />
    </>
  )
}

const List = styled.div`
  list-style-type: none;
  padding: 25px 0 0;
  margin: 0;

  & > li {
    :not(:last-child) {
      margin-bottom: 15px;
    }
  }
`

RatedMovieList.propTypes = {
  movies: PropTypes.array.isRequired,
  // loading: PropTypes.bool.isRequired,
  pagination: PropTypes.object.isRequired
}

export default RatedMovieList
