import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Button from './common/ButtonLink'
import { connect } from 'react-redux'
import PlaceholderImage from './common/PlaceholderImage'
import { getMovie } from '../selectors'

const MovieItem = ({ movie, baseUrl }) => {
  return (
    <Wrapper>
      <Image>
        <PlaceholderImage
          baseUrl={baseUrl}
          path={movie.poster_path}
          alt={movie.title}
        />
      </Image>
      <Content>
        <Title>{movie.title}</Title>
        <Date>{movie.release_date}</Date>
        <Overview>{movie.overview}</Overview>
        <Button href={`/movies/${movie.id}`}>More</Button>
      </Content>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
`

const Image = styled.div`
  flex: 0 0 40%;
  padding-right: 15px;
  box-sizing: border-box;

  img {
    display: block;
    max-width: 100%;
    height: auto;
  }
`

const Content = styled.div`
  flex: 0 0 60%;
`

const Title = styled.div`
  font-size: 20px;
  margin-bottom: 5px;
`

const Date = styled.div`
  font-size: 13px;
  color: #42526e;
  margin-bottom: 10px;
`

const Overview = styled.div`
  font-size: 12px;
  color: #6b778c;
  margin-bottom: 15px;
`

MovieItem.propTypes = {
  movie: PropTypes.object.isRequired,
  baseUrl: PropTypes.string.isRequired
}

export default connect((state, props) => ({
  movie: getMovie(state, props.id),
  baseUrl: state.global.baseUrl
}))(MovieItem)
