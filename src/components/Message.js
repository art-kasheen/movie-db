import React from 'react'
import PropTypes from 'prop-types'
import SuccessIcon from '@atlaskit/icon/glyph/check-circle'
import ErrorIcon from '@atlaskit/icon/glyph/error'
import { colors } from '@atlaskit/theme'
import { connect } from 'react-redux'
import { clearMessage } from '../actions'
import Flag, { AutoDismissFlag, FlagGroup } from '@atlaskit/flag'

const getIcon = (type) => {
  switch (type) {
    case 'error':
      return <ErrorIcon label="" secondaryColor={colors.R400} />

    case 'success':
      return (
        <SuccessIcon
          label="Success"
          size="medium"
          secondaryColor={colors.G400}
        />
      )

    default:
      return ''
  }
}

const getText = (type, message) => {
  switch (type) {
    case 'error':
      return 'Something went wrong'

    case 'success':
      return message.text

    default:
      return ''
  }
}

const Message = ({ messages, clearMessage }) => {
  if (!messages || !messages.length) return null

  const handleDismiss = () => {
    clearMessage()
  }

  return (
    <FlagGroup onDismissed={handleDismiss}>
      {messages.map((message, index) => (
        <AutoDismissFlag
          appearance={message.type}
          id={index}
          icon={getIcon(message.type)}
          key={index}
          title={getText(message.type, message)}
        />
      ))}
    </FlagGroup>
  )
}

Flag.propTypes = {
  messages: PropTypes.array,
  clearMessage: PropTypes.func
}

export default connect(
  (state) => ({
    messages: state.global.messages.toJS()
  }),
  { clearMessage }
)(Message)
