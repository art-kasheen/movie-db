import React from 'react'
import Spinner from '@atlaskit/spinner'
import styled from 'styled-components'

export default () => {
  return (
    <Wrapper>
      <Spinner size="large" />
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: block;
  width: 48px;
  height: 48px;
  margin: 20px auto;
`
