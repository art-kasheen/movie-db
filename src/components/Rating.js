import React, { useState } from 'react'
import PropTypes from 'prop-types'
import StarIcon from '@atlaskit/icon/glyph/star'
import StarFilledIcon from '@atlaskit/icon/glyph/star-filled'
import CheckboxIndeterminateIcon from '@atlaskit/icon/glyph/checkbox-indeterminate'
import styled from 'styled-components'

const Rating = ({ selected, onChange, onDelete }) => {
  const initialRate = selected
  const [isOver, setOver] = useState(false)
  const [rate, setRate] = useState(initialRate)
  const [hoverRate, setHoverRate] = useState(rate)

  const items = Array.from([2, 4, 6, 8, 10]).map((item) => (
    <li
      data-test="rating-item"
      key={item}
      onClick={() => {
        onChange(item)
        setRate(item)
      }}
      onMouseEnter={() => {
        setOver(true)
        setHoverRate(item)
      }}
    >
      {item <= (isOver ? hoverRate : rate) ? <StarFilledIcon /> : <StarIcon />}
    </li>
  ))

  return (
    <Wrapper>
      <List
        onMouseLeave={() => {
          !isOver && setRate(initialRate)
          setOver(false)
        }}
        data-test="rating-list"
      >
        <li
          onClick={() => {
            onDelete()
            setRate(0)
            setHoverRate(0)
          }}
          data-test="rating-delete"
        >
          <CheckboxIndeterminateIcon />
        </li>
        {items}
      </List>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const List = styled.ul`
  display: inline-flex;
  list-style-type: none;
  padding: 0;
  margin: 0;
  align-items: center;

  li {
    margin: 0;
    cursor: pointer;

    span {
      display: block;
    }
  }
`

Rating.propTypes = {
  selected: PropTypes.number,
  onChange: PropTypes.func
}

Rating.defaultProps = {
  selected: 0
}

export default Rating
