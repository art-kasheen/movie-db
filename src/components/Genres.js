import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getGenres } from '../selectors'
import Button from './common/ButtonLink'
import Loader from './Loader'
import styled from 'styled-components'

const Genres = ({ activeId, genres, loading }) => {
  if (loading) return <Loader />

  return (
    <div>
      <h2>Genres</h2>
      <Wrapper>
        {genres.map((genre) => (
          <Button
            isSelected={activeId && +activeId === genre.id}
            appearance="subtle"
            key={genre.id}
            href={`/genre/${genre.id}`}
          >
            {genre.name}
          </Button>
        ))}
      </Wrapper>
    </div>
  )
}

const Wrapper = styled.div`
  padding: 20px 0 10px;
`

Genres.propTypes = {
  genres: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  activeId: PropTypes.string
}

export default connect((state) => ({
  genres: getGenres(state),
  loading: state.genres.loading
}))(Genres)
