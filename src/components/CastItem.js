import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import PlaceholderImage from './common/PlaceholderImage'

const CastItem = ({ baseUrl, person }) => {
  return (
    <Wrapper>
      <Link to={`/person/${person.id}`}>
        <Image>
          <PlaceholderImage
            baseUrl={baseUrl}
            path={person.profile_path}
            alt={person.name}
          />
        </Image>
        <Title>{person.name}</Title>
        <Character>{person.character}</Character>
      </Link>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  & > a {
    display: block;
    text-align: center;
    text-decoration: none;
    transition: 0.45s ease-out;

    :hover {
      text-decoration: none;
      transform: translateY(-7px);
    }
  }
`

const Image = styled.div`
  margin: 0 auto;
  width: 130px;
  height: 130px;
  border-radius: 50%;
  overflow: hidden;

  img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`

const Title = styled.h3`
  font-size: 16px;
  margin-top: 10px;
`

const Character = styled.p`
  font-size: 12px;
  margin-top: 3px;
  color: #505f79;
`

CastItem.propTypes = {
  person: PropTypes.object.isRequired,
  baseUrl: PropTypes.string.isRequired
}

export default CastItem
