import React from 'react'
import { shallow, mount } from 'enzyme'
import Rating from './Rating'
import StarFilledIcon from '@atlaskit/icon/glyph/star-filled'
import StarIcon from '@atlaskit/icon/glyph/star'

describe('Rating tests', () => {
  test('should call onChange', () => {
    const fn = jest.fn()
    const wrapper = shallow(<Rating onChange={fn} />)
    const item = wrapper.find('[data-test="rating-item"]')

    item.at(2).simulate('click')

    expect(fn).toBeCalled()
  })

  test('should call onDelete', () => {
    const fn = jest.fn()
    const wrapper = shallow(<Rating onChange={() => {}} onDelete={fn} />)
    const item = wrapper.find('[data-test="rating-delete"]')

    item.simulate('click')

    expect(fn).toBeCalledTimes(1)
  })

  test('should render filled stars on click', () => {
    const wrapper = shallow(<Rating onChange={() => {}} />)

    const item = wrapper.find('[data-test="rating-item"]')

    item.at(2).simulate('click')

    expect(wrapper.find(StarFilledIcon).length).toEqual(3)
    expect(wrapper.find(StarIcon).length).toEqual(2)
  })

  test('should render filled stars on hover', () => {
    const wrapper = shallow(<Rating onChange={() => {}} />)

    const item = wrapper.find('[data-test="rating-item"]')

    item.at(3).simulate('mouseEnter')

    expect(wrapper.find(StarFilledIcon).length).toEqual(4)
    expect(wrapper.find(StarIcon).length).toEqual(1)
  })

  test('should render selected stars after mouse leave', () => {
    const wrapper = shallow(<Rating onChange={() => {}} selected={6} />)

    const item = wrapper.find('[data-test="rating-item"]')

    item.at(4).simulate('mouseEnter')

    expect(wrapper.find(StarFilledIcon).length).toEqual(5)

    wrapper.find('[data-test="rating-list"]').simulate('mouseLeave')

    expect(wrapper.find(StarFilledIcon).length).toEqual(3)
    expect(wrapper.find(StarIcon).length).toEqual(2)
  })

  test('should render filled stars on selected props', () => {
    const wrapper = shallow(<Rating onChange={() => {}} selected={4} />)

    expect(wrapper.find(StarFilledIcon).length).toEqual(2)
    expect(wrapper.find(StarIcon).length).toEqual(3)
  })

  test('should remove rating stars on reset', () => {
    const wrapper = shallow(
      <Rating onChange={() => {}} onDelete={() => {}} selected={6} />
    )
    const deleteItem = wrapper.find('[data-test="rating-delete"]')

    expect(wrapper.find(StarFilledIcon).length).toEqual(3)
    expect(wrapper.find(StarIcon).length).toEqual(2)

    deleteItem.simulate('click')

    expect(wrapper.find(StarIcon).length).toEqual(5)
  })
})
