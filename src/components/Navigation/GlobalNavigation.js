import React from 'react'
import { GlobalNav } from '@atlaskit/navigation-next'
import SearchIcon from '@atlaskit/icon/glyph/search'
import Avatar from '@atlaskit/avatar'
import Drawer from '@atlaskit/drawer'
import Search from '../Search'
import { connect } from 'react-redux'
import { toggleSearch } from '../../actions'
import { isAuthenticated } from '../../selectors'
import { history } from '../../store'

const globalNavPrimaryItems = (onSearchClick) => [
  { id: 'search', icon: SearchIcon, label: 'Search', onClick: onSearchClick }
]

const GlobalNavigation = ({ isAuthenticated, isDrawerOpen, toggleSearch }) => {
  const openDrawer = () => toggleSearch(true)
  const closeDrawer = () => toggleSearch(false)

  const globalNavSecondaryItems = [
    {
      id: 'user-nav',
      icon: () => (
        <Avatar
          borderColor="transparent"
          isActive={false}
          isHover={false}
          size="small"
          presence={isAuthenticated ? 'online' : 'offline'}
        />
      ),
      label: 'Profile',
      size: 'small',
      onClick: () => history.push('/dashboard/watchlist')
    }
  ]

  return (
    <div>
      <GlobalNav
        primaryItems={globalNavPrimaryItems(openDrawer)}
        secondaryItems={globalNavSecondaryItems}
      />
      <Drawer onClose={closeDrawer} isOpen={isDrawerOpen} width="wide">
        <Search />
      </Drawer>
    </div>
  )
}

export default connect(
  (state) => ({
    isAuthenticated: isAuthenticated(state),
    isDrawerOpen: state.global.isSearchActive
  }),
  { toggleSearch }
)(GlobalNavigation)
