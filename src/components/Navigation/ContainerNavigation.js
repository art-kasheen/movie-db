import React from 'react'
import {
  ContainerHeader,
  HeaderSection,
  GroupHeading,
  ItemAvatar,
  Item as ItemComponent,
  MenuSection
} from '@atlaskit/navigation-next'
import GraphBarIcon from '@atlaskit/icon/glyph/graph-bar'
import EmojiSymbolsIcon from '@atlaskit/icon/glyph/emoji/symbols'
import CalendarIcon from '@atlaskit/icon/glyph/calendar'
import VidPlayIcon from '@atlaskit/icon/glyph/vid-play'
import { Link, Route } from 'react-router-dom'

const LinkItem = ({ to, ...props }) => {
  return (
    <Route
      render={({ location: { pathname } }) => (
        <ItemComponent
          component={({ children, className }) => (
            <Link className={className} to={to}>
              {children}
            </Link>
          )}
          isSelected={pathname === to}
          {...props}
        />
      )}
    />
  )
}

const ContentNavigation = () => {
  const menuItems = [
    {
      key: 'now_playing',
      title: 'Now Playing',
      icon: VidPlayIcon
    },
    {
      key: 'popular',
      title: 'Popular',
      icon: EmojiSymbolsIcon
    },
    {
      key: 'top_rated',
      title: 'Top Rated',
      icon: GraphBarIcon
    },
    {
      key: 'upcoming',
      title: 'Upcoming',
      icon: CalendarIcon
    }
  ]

  return (
    <div>
      <HeaderSection>
        {() => (
          <ContainerHeader
            before={(itemState) => (
              <ItemAvatar
                itemState={itemState}
                appearance="square"
                size="large"
              />
            )}
            text="Movies"
            subText="Find and save favorite"
            component={({ children, className }) => (
              <Link className={className} to="/">
                {children}
              </Link>
            )}
          />
        )}
      </HeaderSection>
      <MenuSection>
        {({ className }) => (
          <div className={className}>
            <GroupHeading>Discover</GroupHeading>
            {menuItems.map((item) => {
              return (
                <LinkItem
                  before={item.icon}
                  key={item.key}
                  text={item.title}
                  to={`/discover/${item.key}`}
                />
              )
            })}
          </div>
        )}
      </MenuSection>
    </div>
  )
}

export default ContentNavigation
