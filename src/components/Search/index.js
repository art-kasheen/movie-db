import React from 'react'
import PropsTypes from 'prop-types'
import SearchForm from './SearchForm'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { searchRequest } from '../../actions'

const Search = ({ searchRequest }) => {
  const handleSubmit = ({ text }) => {
    searchRequest(text)
  }

  return (
    <Wrapper>
      <SearchForm onSubmit={handleSubmit} />
    </Wrapper>
  )
}

SearchForm.propTypes = {
  searchRequest: PropsTypes.func
}

const Wrapper = styled.div`
  margin-right: 40px;
`

export default connect(
  null,
  { searchRequest }
)(Search)
