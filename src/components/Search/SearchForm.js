import React from 'react'
import PropsTypes from 'prop-types'
import Button from '@atlaskit/button'
import styled from 'styled-components'
import { Field, reduxForm } from 'redux-form'
import Input from '../common/FormInput'

const SearchForm = ({ handleSubmit }) => {
  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="search">Search for a movie...</label>
      <Row>
        <Field name="text" type="text" component={Input} />
      </Row>
      <Button type="submit">Submit</Button>
    </form>
  )
}

SearchForm.propTypes = {
  handleSubmit: PropsTypes.func.isRequired
}

const Row = styled.div`
  margin-bottom: 15px;
`

export default reduxForm({
  form: 'search'
})(SearchForm)
