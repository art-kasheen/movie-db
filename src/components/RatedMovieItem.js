import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { getMovie, isAuthenticated } from '../selectors'
import Button from '@atlaskit/button'
import PlaceholderImage from './common/PlaceholderImage'
import MovieActions from './MovieActions'

const RatedMovieItem = ({ isAuthenticated, baseUrl, movie }) => {
  if (!movie) return null

  const getActions = () => isAuthenticated && <MovieActions movie={movie} />

  return (
    <Wrapper>
      <Image>
        <PlaceholderImage
          baseUrl={baseUrl}
          path={movie.poster_path}
          alt={movie.title}
        />
      </Image>
      <Content>
        <Title>{movie.title}</Title>
        <Date>{movie.release_date}</Date>
        <Overview>{movie.overview}</Overview>
        {getActions()}
        <MoreButton href={`/movies/${movie.id}`}>More</MoreButton>
      </Content>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
`

const Image = styled.div`
  flex: 0 0 20%;
  padding-right: 15px;
  box-sizing: border-box;

  img {
    display: block;
    max-width: 100%;
    height: auto;
  }
`

const Content = styled.div`
  flex: 0 0 80%;
`

const Title = styled.div`
  font-size: 20px;
  margin-bottom: 5px;
`

const Date = styled.div`
  font-size: 13px;
  color: #42526e;
  margin-bottom: 10px;
`

const Overview = styled.div`
  font-size: 12px;
  color: #6b778c;
  margin-bottom: 15px;
`

const MoreButton = styled(Button)`
  margin-top: 30px;
`

RatedMovieItem.propTypes = {
  movie: PropTypes.object.isRequired,
  baseUrl: PropTypes.string.isRequired
}

export default connect((state, props) => ({
  isAuthenticated: isAuthenticated(state),
  movie: getMovie(state, props.id),
  baseUrl: state.global.baseUrl
}))(RatedMovieItem)
