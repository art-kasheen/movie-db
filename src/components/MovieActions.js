import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  fetchMovieStatus,
  setMovieRating,
  markMovieFavorite,
  toggleWatchlist
} from '../actions'
import Button from '@atlaskit/button'
import EmojiSymbolsIcon from '@atlaskit/icon/glyph/emoji/symbols'
import WatchIcon from '@atlaskit/icon/glyph/watch'
import Rating from './Rating'
import styled from 'styled-components'

const MovieActions = ({
  movie,
  fetchMovieStatus,
  setMovieRating,
  markMovieFavorite,
  toggleWatchlist
}) => {
  useEffect(() => {
    fetchMovieStatus({ id: movie.id })
    // eslint-disable-next-line
  }, [])

  const movieState = movie.state && movie.state.toJS()

  if (!movieState) return null

  const handleRatingChange = (value) => {
    setMovieRating({ type: 'set', id: movie.id, value })
  }

  const handleRatingDelete = () => {
    setMovieRating({ type: 'delete', id: movie.id, value: 0 })
  }

  const handleFavorite = () => {
    markMovieFavorite({
      id: movie.id,
      value: movieState.favorite ? false : true,
      type: 'favorite'
    })
  }

  const handleWatchlist = () => {
    toggleWatchlist({
      id: movie.id,
      value: movieState.watchlist ? false : true,
      type: 'watchlist'
    })
  }

  return (
    <Actions>
      <Rating
        onChange={handleRatingChange}
        onDelete={handleRatingDelete}
        selected={movieState.rated ? movieState.rated.value : 0}
      />
      <Button
        appearance="primary"
        onClick={handleFavorite}
        isSelected={movieState.favorite}
        iconBefore={<EmojiSymbolsIcon />}
        isLoading={movie.stateLoading === 'favorite'}
      >
        {movieState.favorite ? 'Remove from favorite' : 'Mark as favorite'}
      </Button>
      <Button
        appearance="danger"
        onClick={handleWatchlist}
        iconBefore={<WatchIcon />}
        isSelected={movieState.watchlist}
        isLoading={movie.stateLoading === 'watchlist'}
      >
        {movieState.watchlist ? 'Remove from watchlist' : 'Add to watchlist'}
      </Button>
    </Actions>
  )
}

const Actions = styled.div`
  display: flex;
  align-items: center;
  padding-top: 20px;

  & > * {
    &:not(:last-child) {
      margin-right: 7px;
    }
  }
`

MovieActions.propTypes = {
  movie: PropTypes.object.isRequired,
  fetchMovieStatus: PropTypes.func.isRequired,
  setMovieRating: PropTypes.func.isRequired,
  markMovieFavorite: PropTypes.func.isRequired,
  toggleWatchlist: PropTypes.func.isRequired
}

export default connect(
  null,
  { fetchMovieStatus, setMovieRating, markMovieFavorite, toggleWatchlist }
)(MovieActions)
