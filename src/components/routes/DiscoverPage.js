import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import queryString from 'query-string'
import { connect } from 'react-redux'
import { fetchDiscoverMovies } from '../../actions'
import { getMovies } from '../../selectors'
import MoviesList from '../MoviesList'
import Genres from '../Genres'
import Loader from '../Loader'
import { Container } from '../../styles/Layout'

const DiscoverPage = ({
  fetchDiscoverMovies,
  match,
  location,
  movies,
  loaded,
  pagination
}) => {
  const [pageTitle, setPageTitle] = useState('')
  const { page } = queryString.parse(location.search)

  useEffect(() => {
    fetchDiscoverMovies({
      type: match.params.type,
      page
    })
    // eslint-disable-next-line
  }, [match.params.type, page])

  useEffect(() => {
    switch (match.params.type) {
      case 'popular':
        setPageTitle('Popular')
        break

      case 'now_playing':
        setPageTitle('Now playing')
        break

      case 'top_rated':
        setPageTitle('Top Rated')
        break

      case 'upcoming':
        setPageTitle('Upcoming')
        break

      default:
        setPageTitle('')
    }
  }, [match.params.type])

  return (
    <Container>
      <Genres />
      <h2>{pageTitle} movies</h2>
      {loaded ? (
        <MoviesList movies={movies} pagination={pagination} />
      ) : (
        <Loader />
      )}
    </Container>
  )
}

DiscoverPage.propTypes = {
  fetchDiscoverMovies: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    movies: getMovies(state),
    loaded: state.movies.loaded,
    pagination: state.movies.pagination
  }),
  { fetchDiscoverMovies }
)(DiscoverPage)
