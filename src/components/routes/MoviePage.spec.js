import React from 'react'
import { shallow, mount, render } from 'enzyme'
import { MoviePage } from './MoviePage'
import { movies } from '../../mocks/movies'
import Loader from '../Loader'

describe('Movie Page', () => {
  const props = {
    movie: movies.results[0],
    pagination: {},
    baseUrl: '',
    isAuthenticated: true,
    location: {},
    match: { params: { id: 1 } }
  }

  test('should render loader', () => {
    const nextProps = {
      ...props,
      movie: {
        ...props.movie,
        isFullLoaded: false,
        loading: true
      }
    }

    const wrapper = shallow(<MoviePage {...nextProps} />)

    expect(wrapper.contains(<Loader />)).toBe(true)
  })

  test('should render title with rating', () => {
    const nextProps = {
      ...props,
      movie: {
        ...props.movie,
        isFullLoaded: true,
        loading: false
      }
    }

    const wrapper = shallow(<MoviePage {...nextProps} />)

    expect(wrapper.find('[data-test="movie-title"]').text()).toEqual(
      'The Lion King (7.2)'
    )
  })
})
