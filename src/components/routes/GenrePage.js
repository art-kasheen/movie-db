import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import queryString from 'query-string'
import { connect } from 'react-redux'
import { fetchMoviesByGenre } from '../../actions'
import { getMovies } from '../../selectors'
import Genres from '../Genres'
import MoviesList from '../MoviesList'
import Select from '@atlaskit/select'
import Loader from '../Loader'
import { Container } from '../../styles/Layout'

const GenrePage = ({
  match,
  location,
  history,
  fetchMoviesByGenre,
  movies,
  loaded,
  pagination
}) => {
  const query = queryString.parse(location.search)
  const { id } = match.params

  useEffect(() => {
    fetchMoviesByGenre({
      id,
      page: query.page,
      sort: query.sort
    })
    // eslint-disable-next-line
  }, [id, query.page, query.sort])

  const handleSelectChange = (ev) => {
    query.sort = ev.value
    history.push(`${location.pathname}?${queryString.stringify(query)}`)
  }

  return (
    <Container>
      <Genres activeId={id} />
      <Select
        className="single-select"
        classNamePrefix="react-select"
        options={[
          { label: 'Popularity', value: 'popularity.desc' },
          { label: 'Title', value: 'original_title.desc' },
          { label: 'Date', value: 'release_date.desc' },
          { label: 'Votes', value: 'vote_average.desc' }
        ]}
        placeholder="Sort by"
        onChange={handleSelectChange}
      />
      {loaded ? (
        <MoviesList movies={movies} pagination={pagination} />
      ) : (
        <Loader />
      )}
    </Container>
  )
}

GenrePage.propTypes = {
  fetchMoviesByGenre: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    movies: getMovies(state),
    loaded: state.movies.loaded,
    pagination: state.movies.pagination
  }),
  { fetchMoviesByGenre }
)(GenrePage)
