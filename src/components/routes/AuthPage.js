import React from 'react'
import PropTypes from 'prop-types'
import { Route } from 'react-router-dom'
import { connect } from 'react-redux'
import { signIn } from '../../actions'
import SignInForm from '../SignInForm'

const AuthPage = ({ signIn, loading }) => {
  const handleSignIn = ({ username, password }) => signIn(username, password)

  return (
    <div>
      <Route
        path="/auth/signin"
        render={() => <SignInForm loading={loading} onSubmit={handleSignIn} />}
      />
    </div>
  )
}

AuthPage.propTypes = {
  signIn: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
}

export default connect(
  (state) => ({
    loading: state.auth.loading
  }),
  { signIn }
)(AuthPage)
