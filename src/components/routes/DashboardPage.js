import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getUser } from '../../selectors'
import { logOut } from '../../actions'
import { Route, Redirect } from 'react-router-dom'
import { ButtonGroup } from '@atlaskit/button'
import Button from '../common/ButtonLink'
import Watchlist from '../Dashboard/Watchlist'
import { Container } from '../../styles/Layout'
import styled from 'styled-components'

const DashboardPage = ({ location, user, logOut, loading }) => {
  return (
    <Container>
      <InfoLine>
        <Avatar>{user.username[0]}</Avatar>
        <Username>Hi {user.username}</Username>
        <LogOutButton onClick={() => logOut()} isLoading={loading}>
          Logout
        </LogOutButton>
      </InfoLine>

      <ButtonGroup appearance="primary">
        <Button
          href="/dashboard/watchlist"
          isSelected={location.pathname === '/dashboard/watchlist'}
        >
          Watchlist
        </Button>
        <Button
          href="/dashboard/favorite"
          isSelected={location.pathname === '/dashboard/favorite'}
        >
          Favorites
        </Button>
        <Button
          href="/dashboard/rated"
          isSelected={location.pathname === '/dashboard/rated'}
        >
          Rated
        </Button>
      </ButtonGroup>
      <Redirect from="/dashboard" to="/dashboard/watchlist" />
      <Route path="/dashboard/:list" exact component={Watchlist} />
    </Container>
  )
}

const InfoLine = styled.div`
  padding: 60px 25px;
  display: flex;
  align-items: center;
  background: #f4f5f7;
  border-radius: 5px;
  margin-bottom: 30px;
`

const LogOutButton = styled(Button)`
  margin-left: auto;
`

const Username = styled.p`
  font-size: 36px;
  color: #42516e;
  margin: 0;
`

const Avatar = styled.div`
  height: 100px;
  width: 100px;
  border-radius: 50%;
  background: #d40142;
  font-size: 40px;
  font-weight: 700;
  color: #ffffff;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-transform: uppercase;
  margin-right: 30px;
`

DashboardPage.propTypes = {
  user: PropTypes.object,
  logOut: PropTypes.func
}

export default connect(
  (state) => ({
    user: getUser(state),
    loading: state.auth.loading
  }),
  { logOut }
)(DashboardPage)
