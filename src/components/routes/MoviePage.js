import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { ButtonGroup } from '@atlaskit/button'
import Button from '../common/ButtonLink'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { fetchMovie, fetchSimilarMovies } from '../../actions'
import { getMovie, isAuthenticated } from '../../selectors'
import queryString from 'query-string'
import PlaceholderImage from '../common/PlaceholderImage'
import { getFormatedTime } from '../../services/helpers'
import Loader from '../Loader'
import Cast from '../Cast'
import MoviesList from '../MoviesList'
import MovieActions from '../MovieActions'
import { Container } from '../../styles/Layout'

export const MoviePage = ({
  match,
  movie,
  pagination,
  baseUrl,
  location,
  fetchMovie,
  fetchSimilarMovies,
  isAuthenticated
}) => {
  const { page } = queryString.parse(location.search)

  useEffect(() => {
    if (movie && movie.isFullLoaded) return
    fetchMovie(match.params.id)
    // eslint-disable-next-line
  }, [match.params.id])

  useEffect(() => {
    movie &&
      movie.isFullLoaded &&
      fetchSimilarMovies({
        id: match.params.id,
        page
      })
    // eslint-disable-next-line
  }, [movie && movie.isFullLoaded, location])

  if (!movie || movie.loading || !movie.isFullLoaded) return <Loader />

  const similar = movie.similar && movie.similar.toJS()

  const getGenres = () => {
    return (
      <ButtonGroup appearance="default">
        {movie.genres &&
          movie.genres.map((genre) => (
            <Button key={genre.id} href={`/genre/${genre.id}`}>
              {genre.name}
            </Button>
          ))}
      </ButtonGroup>
    )
  }

  const getActions = () => isAuthenticated && <MovieActions movie={movie} />

  return (
    <Container>
      <TopPart>
        <Image>
          <PlaceholderImage
            baseUrl={baseUrl}
            path={movie.poster_path}
            alt={movie.title}
          />
        </Image>
        <Content>
          <h2 data-test="movie-title">{`${movie.title} (${
            movie.vote_average
          })`}</h2>
          <Tagline>{movie.tagline}</Tagline>
          {getActions()}
          <h3>Genres</h3>
          {getGenres()}
          <h3>Facts</h3>
          <Info>
            <InfoItem>Released: {movie.release_date}</InfoItem>
            <InfoItem>Budget: {`$ ${movie.budget}`}</InfoItem>
            <InfoItem>Runtime: {getFormatedTime(movie.runtime)}</InfoItem>
          </Info>
          <h3>Overview</h3>
          <Overview>{movie.overview}</Overview>
        </Content>
      </TopPart>
      <ContentTitle>Top Cast</ContentTitle>
      <Cast movieId={movie.id} />

      {similar && similar.length ? (
        <>
          <ContentTitle>Similar movies</ContentTitle>
          {movie.similarLoaded ? (
            <MoviesList movies={similar} pagination={pagination} />
          ) : (
            <Loader />
          )}
        </>
      ) : null}
    </Container>
  )
}

const TopPart = styled.div`
  display: flex;
`

const Image = styled.div`
  flex: 0 0 30%;

  img {
    display: block;
    max-width: 100%;
    height: auto;
  }
`

const Content = styled.div`
  flex: 0 0 70%;
  padding-left: 20px;
  box-sizing: border-box;
`

const ContentTitle = styled.h2`
  margin-bottom: 25px;
`

const Tagline = styled.p``

const Overview = styled.p`
  font-size: 15px;
  color: #42526e;
`

const Info = styled.div`
  display: flex;
  align-items: baseline;
`

const InfoItem = styled.p`
  font-size: 14px;
  color: #0052cc;
  margin-right: 20px;
`

MoviePage.propTypes = {
  movie: PropTypes.object,
  pagination: PropTypes.object,
  baseUrl: PropTypes.string.isRequired,
  isAuthenticated: PropTypes.bool,
  fetchMovie: PropTypes.func,
  fetchSimilarMovies: PropTypes.func
}

export default connect(
  (state, props) => ({
    movie: getMovie(state, props.match.params.id),
    pagination: state.movies.pagination,
    baseUrl: state.global.baseUrl,
    isAuthenticated: isAuthenticated(state)
  }),
  { fetchMovie, fetchSimilarMovies }
)(MoviePage)
