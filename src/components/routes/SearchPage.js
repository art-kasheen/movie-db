import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import MoviesList from '../MoviesList'
import { connect } from 'react-redux'
import { getMovies } from '../../selectors'
import { fetchSearchMovies } from '../../actions'
import Loader from '../Loader'
import queryString from 'query-string'
import { Container } from '../../styles/Layout'
import styled from 'styled-components'

const SearchPage = ({
  match,
  location,
  fetchSearchMovies,
  movies,
  loaded,
  pagination
}) => {
  const { page } = queryString.parse(location.search)

  useEffect(() => {
    fetchSearchMovies({
      query: match.params.query,
      page
    })
    // eslint-disable-next-line
  }, [location])

  return (
    <Container>
      <Title>Search results for "{match.params.query}"</Title>
      {loaded ? (
        <MoviesList movies={movies} pagination={pagination} />
      ) : (
        <Loader />
      )}
    </Container>
  )
}

const Title = styled.h2`
  margin-bottom: 25px;
`

SearchPage.propTypes = {
  movies: PropTypes.array.isRequired,
  loaded: PropTypes.bool.isRequired,
  pagination: PropTypes.object,
  match: PropTypes.object,
  fetchSearchMovies: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    movies: getMovies(state),
    loaded: state.movies.loaded,
    pagination: state.movies.pagination
  }),
  { fetchSearchMovies }
)(SearchPage)
