import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchPerson, fetchPersonMovies } from '../../actions'
import { getPerson } from '../../selectors'
import styled from 'styled-components'
import queryString from 'query-string'
import PlaceholderImage from '../common/PlaceholderImage'
import Loader from '../Loader'
import MoviesList from '../MoviesList'
import { Container } from '../../styles/Layout'

const PersonPage = ({
  baseUrl,
  person,
  fetchPerson,
  fetchPersonMovies,
  match,
  pagination,
  loading,
  location
}) => {
  const { page } = queryString.parse(location.search)

  useEffect(() => {
    if (person && person.loaded) return
    fetchPerson(match.params.id)
    // eslint-disable-next-line
  }, [match.params.id])

  useEffect(() => {
    person &&
      person.loaded &&
      fetchPersonMovies({
        id: match.params.id,
        page
      })
    // eslint-disable-next-line
  }, [person && person.loaded, location])

  if (!person || person.loading || !person.loaded) return <Loader />

  const movies = person.movies && person.movies.toJS()

  return (
    <Container>
      <TopPart>
        <Image>
          <PlaceholderImage
            baseUrl={baseUrl}
            path={person.profile_path}
            alt={person.name}
          />
        </Image>
        <Content>
          <Title>{person.name}</Title>
          <Label>Birthday</Label>
          <Birthday>{person.birthday}</Birthday>
          <Label>Deathday</Label>
          <Deathday>{person.deathday ? person.deathday : '-'}</Deathday>
          <Label>Biography</Label>
          <Biography>{person.biography}</Biography>
        </Content>
      </TopPart>

      {movies && movies.length ? (
        <>
          <h2>Actor Movies</h2>
          {person.moviesLoaded ? (
            <MoviesList movies={movies} pagination={pagination} />
          ) : (
            <Loader />
          )}
        </>
      ) : null}
    </Container>
  )
}

const TopPart = styled.div`
  display: flex;
`

const Image = styled.div`
  flex: 0 0 30%;
  padding-right: 25px;
  box-sizing: border-box;

  img {
    display: block;
    max-width: 100%;
    height: auto;
  }
`

const Content = styled.div`
  flex: 0 0 70%;
`

const Title = styled.h2``
const Birthday = styled.span``
const Deathday = styled.span``
const Biography = styled.p`
  margin-top: 0;
`
const Label = styled.p`
  font-size: 12px;
  color: #6b778c;
  text-transform: uppercase;
`

PersonPage.propTypes = {
  fetchPerson: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  person: PropTypes.object,
  baseUrl: PropTypes.string.isRequired
}

export default connect(
  (state, props) => ({
    person: getPerson(state, props.match.params.id),
    baseUrl: state.global.baseUrl,
    loading: state.movies.loading,
    pagination: state.movies.pagination
  }),
  { fetchPerson, fetchPersonMovies }
)(PersonPage)
