import { Record } from 'immutable'
import {
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  LOGOUT_SUCCESS,
  LOGOUT_REQUEST
} from '../constants'

const ReducerSchema = Record({
  user: null,
  sessionId: null,
  loading: false
})

export default (state = new ReducerSchema(), action) => {
  const { type, payload } = action

  switch (type) {
    case LOGOUT_REQUEST:
    case SIGN_IN_REQUEST:
      return state.set('loading', true)

    case SIGN_IN_SUCCESS:
      return state
        .set('user', payload.user)
        .set('sessionId', payload.sessionId)
        .set('loading', false)

    case LOGOUT_SUCCESS:
      return state
        .set('user', null)
        .set('sessionId', null)
        .set('loading', false)

    default:
      return state
  }
}
