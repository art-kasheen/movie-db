import {
  APP_INIT_SUCCESS,
  FETCH_CONFIG_SUCCESS,
  TOGGLE_SEARCH,
  CLEAR_MESSAGE,
  SET_MESSAGE
} from '../constants'
import { Record, List } from 'immutable'

const ReducerSchema = Record({
  config: null,
  baseUrl: null,
  isSearchActive: false,
  initialized: false,
  messages: new List([])
})

export default (state = new ReducerSchema(), { type, payload }) => {
  switch (type) {
    case APP_INIT_SUCCESS:
      return state.set('initialized', true)

    case FETCH_CONFIG_SUCCESS:
      return state.set('baseUrl', payload.images.base_url)

    case TOGGLE_SEARCH:
      return state.set('isSearchActive', payload)

    case SET_MESSAGE:
      return state.update('messages', (messages) => messages.unshift(payload))

    case CLEAR_MESSAGE:
      return state.update('messages', (messages) => messages.shift())

    default:
      return state
  }
}
