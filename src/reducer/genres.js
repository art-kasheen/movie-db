import {
  FETCH_MOVIES_GENRES_REQUEST,
  FETCH_MOVIES_GENRES_SUCCESS
} from '../constants'
import { Record, Map } from 'immutable'
import { toMap } from '../services/utils'

const ReducerSchema = Record({
  entities: new Map({}),
  loading: false
})

const GenreSchema = Record({
  id: null,
  name: null
})

export default (state = new ReducerSchema(), action) => {
  const { type, payload } = action

  switch (type) {
    case FETCH_MOVIES_GENRES_REQUEST:
      return state.set('loading', true)

    case FETCH_MOVIES_GENRES_SUCCESS:
      return state
        .set('entities', toMap(payload.genres, GenreSchema))
        .set('loading', false)

    default:
      return state
  }
}
