import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { reducer as formReducer } from 'redux-form'
import auth from './auth'
import movies from './movies'
import global from './global'
import person from './person'
import genres from './genres'

const reducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    form: formReducer,
    global,
    auth,
    movies,
    person,
    genres
  })

export default reducer
