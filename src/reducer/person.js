import {
  FETCH_PERSON_REQUEST,
  FETCH_PERSON_SUCCESS,
  FETCH_PERSON_MOVIES_SUCCESS,
  FETCH_PERSON_MOVIES_REQUEST
} from '../constants'
import { Record, Map, List } from 'immutable'

const ReducerSchema = Record({
  entities: new Map({})
})

const PersonSchema = Record({
  id: null,
  name: null,
  biography: null,
  birthday: null,
  deathday: null,
  profile_path: null,
  loading: false,
  loaded: false,
  movies: null,
  moviesLoaded: false
})

export default (state = new ReducerSchema(), action) => {
  const { type, payload } = action

  switch (type) {
    case FETCH_PERSON_REQUEST:
      return state
        .setIn(['entities', Number(payload), 'loading'], true)
        .setIn(['entities', Number(payload), 'loaded'], false)

    case FETCH_PERSON_SUCCESS:
      return state
        .setIn(['entities', Number(payload.id)], new PersonSchema(payload))
        .setIn(['entities', Number(payload.id), 'loading'], false)
        .setIn(['entities', Number(payload.id), 'loaded'], true)

    case FETCH_PERSON_MOVIES_REQUEST:
      return state.setIn(
        ['entities', Number(payload.id), 'moviesLoaded'],
        false
      )

    case FETCH_PERSON_MOVIES_SUCCESS:
      return state
        .setIn(
          ['entities', Number(payload.id), 'movies'],
          List(payload.data.results).map((item) => item.id)
        )
        .setIn(['entities', Number(payload.id), 'moviesLoaded'], true)

    default:
      return state
  }
}
