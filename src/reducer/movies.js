import {
  FETCH_SEARCH_MOVIES_REQUEST,
  FETCH_SEARCH_MOVIES_SUCCESS,
  FETCH_MOVIE_REQUEST,
  FETCH_MOVIE_SUCCESS,
  FETCH_MOVIE_CAST_SUCCESS,
  FETCH_SIMILAR_MOVIES_REQUEST,
  FETCH_SIMILAR_MOVIES_SUCCESS,
  FETCH_PERSON_MOVIES_SUCCESS,
  FETCH_PERSON_MOVIES_REQUEST,
  FETCH_DISCOVER_MOVIES_REQUEST,
  FETCH_DISCOVER_MOVIES_SUCCESS,
  FETCH_MOVIES_BY_GENRE_REQUEST,
  FETCH_MOVIES_BY_GENRE_SUCCESS,
  FETCH_WATCHLIST_REQUEST,
  FETCH_WATCHLIST_SUCCESS,
  FETCH_MOVIE_STATUS_SUCCESS,
  MARK_MOVIE_FAVORITE_SUCCESS,
  TOGGLE_MOVIE_WATCHLIST_SUCCESS,
  MARK_MOVIE_FAVORITE_REQUEST,
  TOGGLE_MOVIE_WATCHLIST_REQUEST
} from '../constants'
import { Record, OrderedMap, Map, List } from 'immutable'
import { toMap } from '../services/utils'

export const ReducerSchema = Record({
  entities: new OrderedMap({}),
  loading: false,
  loaded: false,
  pagination: null
})

const CastSchema = Record({
  id: null,
  order: null,
  name: null,
  profile_path: null,
  character: null
})

export const MovieSchema = Record({
  id: null,
  title: null,
  overview: null,
  poster_path: null,
  release_date: null,
  genres: null,
  budget: null,
  runtime: null,
  tagline: null,
  vote_average: null,
  cast: new OrderedMap({}),
  state: null,
  stateLoaded: false,
  stateLoading: null,
  loading: false,
  isFullLoaded: false,
  similar: null,
  similarLoaded: false
})

export default (state = new ReducerSchema(), action) => {
  const { type, payload } = action

  switch (type) {
    case FETCH_DISCOVER_MOVIES_REQUEST:
    case FETCH_SEARCH_MOVIES_REQUEST:
    case FETCH_PERSON_MOVIES_REQUEST:
    case FETCH_MOVIES_BY_GENRE_REQUEST:
    case FETCH_WATCHLIST_REQUEST:
      return state.set('loading', true).set('loaded', false)

    case FETCH_DISCOVER_MOVIES_SUCCESS:
    case FETCH_SEARCH_MOVIES_SUCCESS:
    case FETCH_MOVIES_BY_GENRE_SUCCESS:
    case FETCH_WATCHLIST_SUCCESS:
      return state
        .setIn(['entities'], toMap(payload.results, MovieSchema))
        .set(
          'pagination',
          new Map({
            page: payload.page,
            pages: payload.total_pages,
            total: payload.total_results
          })
        )
        .set('loading', false)
        .set('loaded', true)

    case FETCH_MOVIE_REQUEST:
      return state.setIn(['entities', Number(payload.id), 'loading'], true)

    case FETCH_MOVIE_SUCCESS:
      return state
        .setIn(['entities', Number(payload.id)], new MovieSchema(payload))
        .setIn(['entities', Number(payload.id), 'isFullLoaded'], true)
        .setIn(['entities', Number(payload.id), 'loading'], false)

    case FETCH_MOVIE_CAST_SUCCESS:
      return state.setIn(
        ['entities', payload.movieId, 'cast'],
        toMap(payload.data.cast, CastSchema)
      )

    case FETCH_SIMILAR_MOVIES_REQUEST:
      return state.setIn(
        ['entities', Number(payload.id), 'similarLoaded'],
        false
      )

    case FETCH_SIMILAR_MOVIES_SUCCESS:
      return state
        .mergeIn(['entities'], toMap(payload.data.results, MovieSchema))
        .setIn(
          ['entities', Number(payload.id), 'similar'],
          List(payload.data.results).map((item) => item.id)
        )
        .set(
          'pagination',
          new Map({
            page: payload.data.page,
            pages: payload.data.total_pages,
            total: payload.data.total_results
          })
        )
        .setIn(['entities', Number(payload.id), 'similarLoaded'], true)

    case FETCH_PERSON_MOVIES_SUCCESS:
      return state
        .mergeIn(['entities'], toMap(payload.data.results, MovieSchema))
        .set(
          'pagination',
          new Map({
            page: payload.data.page,
            pages: payload.data.total_pages,
            total: payload.data.total_results
          })
        )
        .set('loading', false)

    case FETCH_MOVIE_STATUS_SUCCESS:
      return state
        .setIn(
          ['entities', Number(payload.id), 'state'],
          new Map({
            favorite: payload.favorite,
            rated: payload.rated,
            watchlist: payload.watchlist
          })
        )
        .setIn(['entities', Number(payload.id), 'stateLoaded'], true)

    case MARK_MOVIE_FAVORITE_REQUEST:
    case TOGGLE_MOVIE_WATCHLIST_REQUEST:
      return state.setIn(
        ['entities', Number(payload.id), 'stateLoading'],
        payload.type
      )

    case MARK_MOVIE_FAVORITE_SUCCESS:
      return state
        .setIn(
          ['entities', Number(payload.id), 'state', 'favorite'],
          payload.value
        )
        .setIn(['entities', Number(payload.id), 'stateLoading'], null)

    case TOGGLE_MOVIE_WATCHLIST_SUCCESS:
      return state
        .setIn(
          ['entities', Number(payload.id), 'state', 'watchlist'],
          payload.value
        )
        .setIn(['entities', Number(payload.id), 'stateLoading'], null)

    default:
      return state
  }
}
