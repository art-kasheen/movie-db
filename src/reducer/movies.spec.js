import reducer, { ReducerSchema, MovieSchema } from './movies'
import {
  FETCH_SEARCH_MOVIES_REQUEST,
  FETCH_SEARCH_MOVIES_SUCCESS,
  FETCH_MOVIE_SUCCESS,
  FETCH_MOVIE_REQUEST,
  FETCH_SIMILAR_MOVIES_REQUEST,
  FETCH_SIMILAR_MOVIES_SUCCESS,
  MARK_MOVIE_FAVORITE_REQUEST,
  MARK_MOVIE_FAVORITE_SUCCESS,
  FETCH_MOVIE_STATUS_SUCCESS,
  FETCH_PERSON_MOVIES_SUCCESS
} from '../constants'
import { Map, List } from 'immutable'
import { movies } from '../mocks/movies'
import { toMap } from '../services/utils'

const state = new ReducerSchema()

const movie = movies.results[0]

describe('Movies Reducer', () => {
  test('hndle FETCH_SEARCH_MOVIES_REQUEST', () => {
    const newState = reducer(state, {
      type: FETCH_SEARCH_MOVIES_REQUEST,
      payload: movies
    })

    const expectedState = new ReducerSchema()
      .set('loading', true)
      .set('loaded', false)

    expect(expectedState).toEqual(newState)
  })

  test('handle FETCH_SEARCH_MOVIES_SUCCESS', () => {
    const prevState = new ReducerSchema().set('loading', true)

    const newState = reducer(prevState, {
      type: FETCH_SEARCH_MOVIES_SUCCESS,
      payload: movies
    })

    const expectedState = prevState
      .setIn(['entities'], toMap(movies.results, MovieSchema))
      .set(
        'pagination',
        new Map({
          page: movies.page,
          pages: movies.total_pages,
          total: movies.total_results
        })
      )
      .set('loading', false)
      .set('loaded', true)

    expect(expectedState).toEqual(newState)
  })

  test('handle FETCH_MOVIE_REQUEST', () => {
    const state = new ReducerSchema()

    const newState = reducer(state, {
      type: FETCH_MOVIE_REQUEST,
      payload: movie
    })

    const expectedState = new ReducerSchema().setIn(
      ['entities', Number(movie.id), 'loading'],
      true
    )

    expect(expectedState).toEqual(newState)
  })

  test('handle FETCH_MOVIE_SUCCESS', () => {
    const prevState = new ReducerSchema().setIn(
      ['entities', Number(movie.id), 'loading'],
      true
    )

    const newState = reducer(prevState, {
      type: FETCH_MOVIE_SUCCESS,
      payload: movie
    })

    const expectedState = prevState
      .setIn(['entities', Number(movie.id)], new MovieSchema(movie))
      .setIn(['entities', Number(movie.id), 'isFullLoaded'], true)
      .setIn(['entities', Number(movie.id), 'loading'], false)

    expect(expectedState).toEqual(newState)
  })

  test('handle FETCH_SIMILAR_MOVIES_REQUEST', () => {
    const newState = reducer(state, {
      type: FETCH_SIMILAR_MOVIES_REQUEST,
      payload: movie
    })

    const expectedState = new ReducerSchema().setIn(
      ['entities', Number(movie.id), 'similarLoaded'],
      false
    )

    expect(expectedState).toEqual(newState)
  })

  test('handle FETCH_SIMILAR_MOVIES_SUCCESS', () => {
    const prevState = new ReducerSchema().setIn(
      ['entities', Number(movie.id), 'similarLoaded'],
      false
    )

    const newState = reducer(prevState, {
      type: FETCH_SIMILAR_MOVIES_SUCCESS,
      payload: {
        id: movie.id,
        data: movies
      }
    })

    const expectedState = prevState
      .mergeIn(['entities'], toMap(movies.results, MovieSchema))
      .setIn(
        ['entities', Number(movie.id), 'similar'],
        List(movies.results).map((item) => item.id)
      )
      .set(
        'pagination',
        new Map({
          page: movies.page,
          pages: movies.total_pages,
          total: movies.total_results
        })
      )
      .setIn(['entities', Number(movie.id), 'similarLoaded'], true)

    expect(expectedState).toEqual(newState)
  })

  test('handle FETCH_PERSON_MOVIES_SUCCESS', () => {
    const payload = {
      data: movies
    }

    const nextState = reducer(state, {
      type: FETCH_PERSON_MOVIES_SUCCESS,
      payload
    })

    const expectedState = new ReducerSchema()
      .mergeIn(['entities'], toMap(payload.data.results, MovieSchema))
      .set(
        'pagination',
        new Map({
          page: payload.data.page,
          pages: payload.data.total_pages,
          total: payload.data.total_results
        })
      )
      .set('loading', false)

    expect(expectedState).toEqual(nextState)
  })

  test('handle FETCH_MOVIE_STATUS_SUCCESS', () => {
    const payload = {}

    const nextState = reducer(state, {
      type: FETCH_MOVIE_STATUS_SUCCESS,
      payload
    })

    const expectedState = new ReducerSchema()
      .setIn(
        ['entities', Number(payload.id), 'state'],
        new Map({
          favorite: payload.favorite,
          rated: payload.rated,
          watchlist: payload.watchlist
        })
      )
      .setIn(['entities', Number(payload.id), 'stateLoaded'], true)

    expect(expectedState).toEqual(nextState)
  })

  test('handle MARK_MOVIE_FAVORITE_REQUEST', () => {
    const payload = {
      id: movie.id,
      type: 'favorite'
    }

    const nextState = reducer(state, {
      type: MARK_MOVIE_FAVORITE_REQUEST,
      payload
    })

    const expectedState = new ReducerSchema().setIn(
      ['entities', Number(payload.id), 'stateLoading'],
      payload.type
    )

    expect(expectedState).toEqual(nextState)
  })

  test('handle MARK_MOVIE_FAVORITE_SUCCESS', () => {
    const payload = {
      id: movie.id,
      type: 'favorite',
      value: true
    }

    const prevState = new ReducerSchema().setIn(
      ['entities', Number(payload.id), 'stateLoading'],
      payload.type
    )

    const nextState = reducer(state, {
      type: MARK_MOVIE_FAVORITE_SUCCESS,
      payload
    })

    const expectedState = prevState
      .setIn(
        ['entities', Number(payload.id), 'state', 'favorite'],
        payload.value
      )
      .setIn(['entities', Number(payload.id), 'stateLoading'], null)

    expect(expectedState).toEqual(nextState)
  })
})
