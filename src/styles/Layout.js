import styled from 'styled-components'

export const Wrapper = styled.div`
  min-width: 1000px;
`

export const Container = styled.div`
  max-width: 960px;
  margin: 0 auto;
`
