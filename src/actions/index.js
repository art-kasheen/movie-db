import {
  APP_INIT_REQUET,
  SEARCH_REQUEST,
  FETCH_CONFIG_REQUEST,
  FETCH_MOVIE_REQUEST,
  FETCH_MOVIE_STATUS_REQUEST,
  FETCH_MOVIE_CAST_REQUEST,
  FETCH_SEARCH_MOVIES_REQUEST,
  FETCH_DISCOVER_MOVIES_REQUEST,
  FETCH_SIMILAR_MOVIES_REQUEST,
  FETCH_PERSON_MOVIES_REQUEST,
  FETCH_PERSON_REQUEST,
  FETCH_MOVIES_GENRES_REQUEST,
  FETCH_MOVIES_BY_GENRE_REQUEST,
  TOGGLE_SEARCH,
  SIGN_IN_REQUEST,
  LOGOUT_REQUEST,
  FETCH_WATCHLIST_REQUEST,
  AUTH_INIT,
  SET_MOVIE_RATING_REQUEST,
  MARK_MOVIE_FAVORITE_REQUEST,
  TOGGLE_MOVIE_WATCHLIST_REQUEST,
  CLEAR_MESSAGE
} from '../constants'

export function appInit() {
  return {
    type: APP_INIT_REQUET
  }
}

export function authInit() {
  return {
    type: AUTH_INIT
  }
}

export function signIn(username, password) {
  return {
    type: SIGN_IN_REQUEST,
    payload: { username, password }
  }
}

export function logOut() {
  return {
    type: LOGOUT_REQUEST
  }
}

export function searchRequest(payload) {
  return {
    type: SEARCH_REQUEST,
    payload
  }
}

export function fetchWatchList(payload) {
  return {
    type: FETCH_WATCHLIST_REQUEST,
    payload
  }
}

export function fetchSearchMovies(payload) {
  return {
    type: FETCH_SEARCH_MOVIES_REQUEST,
    payload
  }
}

export function fetchMoviesByGenre(payload) {
  return {
    type: FETCH_MOVIES_BY_GENRE_REQUEST,
    payload
  }
}

export function fetchDiscoverMovies(payload) {
  return {
    type: FETCH_DISCOVER_MOVIES_REQUEST,
    payload
  }
}

export function fetchSimilarMovies(payload) {
  return {
    type: FETCH_SIMILAR_MOVIES_REQUEST,
    payload: payload
  }
}

export function fetchMoviesGenres() {
  return {
    type: FETCH_MOVIES_GENRES_REQUEST
  }
}

export function fetchPersonMovies(payload) {
  return {
    type: FETCH_PERSON_MOVIES_REQUEST,
    payload: payload
  }
}

export function fetchMovie(payload) {
  return {
    type: FETCH_MOVIE_REQUEST,
    payload: { id: payload }
  }
}

export function fetchMovieStatus(payload) {
  return {
    type: FETCH_MOVIE_STATUS_REQUEST,
    payload: { id: payload.id, sessionId: payload.sessionId }
  }
}

export function setMovieRating(payload) {
  return {
    type: SET_MOVIE_RATING_REQUEST,
    payload
  }
}

export function markMovieFavorite(payload) {
  return {
    type: MARK_MOVIE_FAVORITE_REQUEST,
    payload
  }
}

export function toggleWatchlist(payload) {
  return {
    type: TOGGLE_MOVIE_WATCHLIST_REQUEST,
    payload
  }
}

export function fecthCast(payload) {
  return {
    type: FETCH_MOVIE_CAST_REQUEST,
    payload: { movieId: payload }
  }
}

export function initConfiguration() {
  return {
    type: FETCH_CONFIG_REQUEST
  }
}

export function toggleSearch(payload) {
  return {
    type: TOGGLE_SEARCH,
    payload
  }
}

export function fetchPerson(id) {
  return {
    type: FETCH_PERSON_REQUEST,
    payload: id
  }
}

export function clearMessage() {
  return {
    type: CLEAR_MESSAGE
  }
}
