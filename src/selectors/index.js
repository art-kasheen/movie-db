import { createSelector } from 'reselect'

export const getId = (state, id) => id
export const getMoviesState = (state) => state.movies
export const getAdminMovies = (state) => state.admin.movies
export const getGenresState = (state) => state.genres
export const getPeople = (state) => state.person
export const getAuth = (state) => state.auth
export const getUser = (state) => state.auth.user
export const isAuthenticated = (state) => !!state.auth.user

export const getMovies = createSelector(
  getMoviesState,
  (movies) =>
    movies.entities
      .valueSeq()
      .toArray()
      .map((item) => item.id)
)

export const getMovie = createSelector(
  getMoviesState,
  getId,
  (movies, id) => {
    return movies.entities.get(Number(id))
  }
)

export const getSimilarMovies = createSelector(
  getMovie,
  (movie) => movie && movie.get('similar') && movie.get('similar').toJS()
)

export const getCast = createSelector(
  getMoviesState,
  getId,
  (movies, id) => {
    return movies.entities
      .getIn([id, 'cast'])
      .valueSeq()
      .toArray()
      .sort((a, b) => (a.order > b.order ? 1 : -1))
      .slice(0, 6)
  }
)

export const getGenres = createSelector(
  getGenresState,
  (genres) => genres.entities.valueSeq().toArray()
)

export const getPerson = createSelector(
  getPeople,
  getId,
  (people, id) => {
    return people.entities.get(Number(id))
  }
)
