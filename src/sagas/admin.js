import API from '../services/api'
import { all, put, call, take, select } from 'redux-saga/effects'
import {
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  SET_MESSAGE
} from '../constants'
import { push } from 'connected-react-router'
import { getAuth } from '../selectors'

export function* logOut() {
  while (true) {
    try {
      yield take(LOGOUT_REQUEST)
      const { sessionId } = yield select(getAuth)

      yield call(API.delete, 'authentication/session', {
        data: {
          session_id: sessionId
        }
      })

      yield localStorage.removeItem('sessionId')
      yield localStorage.removeItem('user')

      yield put({
        type: LOGOUT_SUCCESS
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export function* signIn() {
  while (true) {
    try {
      const { payload } = yield take(SIGN_IN_REQUEST)

      const tokenResult = yield call(API, 'authentication/token/new')
      const validateResult = yield call(
        API.post,
        'authentication/token/validate_with_login',
        {
          request_token: tokenResult.data.request_token,
          username: payload.username,
          password: payload.password
        }
      )
      const sessionResult = yield call(API.post, 'authentication/session/new', {
        request_token: validateResult.data.request_token
      })

      const sessionId = sessionResult.data.session_id

      const accountResult = yield call(API, 'account', {
        params: {
          session_id: sessionId
        }
      })

      const user = accountResult.data

      yield localStorage.setItem('sessionId', sessionId)
      yield localStorage.setItem('user', JSON.stringify(user))

      yield put({
        type: SIGN_IN_SUCCESS,
        payload: {
          user,
          sessionId
        }
      })

      yield put(push('/dashboard'))
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export default function* root() {
  yield all([signIn(), logOut()])
}
