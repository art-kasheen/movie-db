import { fetchPersonSaga } from './person'
import { put, take, call } from 'redux-saga/effects'
import API from '../services/api'
import {
  FETCH_PERSON_REQUEST,
  FETCH_PERSON_SUCCESS,
  SET_MESSAGE
} from '../constants'
import { person } from '../mocks/person'

describe('Person Saga', () => {
  test('should fetch person', () => {
    const saga = fetchPersonSaga()
    const action = {
      type: FETCH_PERSON_REQUEST,
      payload: 1
    }

    expect(saga.next().value).toEqual(take(FETCH_PERSON_REQUEST))

    expect(saga.next(action).value).toEqual(
      call(API, `person/${action.payload}`)
    )

    const result = {
      data: person
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_PERSON_SUCCESS,
        payload: result.data
      })
    )
  })

  test('should get error', () => {
    const saga = fetchPersonSaga()

    saga.next()

    const error = new Error('error')

    expect(saga.throw(error).value).toEqual(
      put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    )
  })
})
