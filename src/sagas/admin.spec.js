import { signIn, logOut } from './admin'
import {
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  SET_MESSAGE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS
} from '../constants'
import { put, take, call, select } from 'redux-saga/effects'
import { push } from 'connected-react-router'
import API from '../services/api'
import { getAuth } from '../selectors'

describe('Admin Saga', () => {
  test('should sign in', () => {
    const saga = signIn()

    const action = {
      type: SIGN_IN_REQUEST,
      payload: { username: 'user', password: '1234' }
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(
      call(API, 'authentication/token/new')
    )

    const tokenResult = {
      data: '1231dfsjnx,l3lm1j45j'
    }

    expect(saga.next(tokenResult).value).toEqual(
      call(API.post, 'authentication/token/validate_with_login', {
        request_token: tokenResult.data.request_token,
        username: action.payload.username,
        password: action.payload.password
      })
    )

    const validateResult = {
      data: '134ggdss'
    }

    expect(saga.next(validateResult).value).toEqual(
      call(API.post, 'authentication/session/new', {
        request_token: validateResult.data.request_token
      })
    )

    const sessionResult = {
      data: {
        session_id: '134ggdss'
      }
    }

    expect(saga.next(sessionResult).value).toEqual(
      call(API, 'account', {
        params: {
          session_id: sessionResult.data.session_id
        }
      })
    )

    expect(saga.next(sessionResult).value).toEqual(
      localStorage.setItem('sessionId', sessionResult.data.session_id)
    )

    const accountResult = {
      data: {
        name: 'user'
      }
    }

    expect(saga.next(accountResult).value).toEqual(
      localStorage.setItem('user', JSON.stringify(accountResult.data))
    )

    expect(saga.next().value).toEqual(
      put({
        type: SIGN_IN_SUCCESS,
        payload: {
          user: sessionResult.data,
          sessionId: sessionResult.data.session_id
        }
      })
    )

    expect(saga.next().value).toEqual(put(push('/dashboard')))
  })

  test('should get error on singn in', () => {
    const saga = signIn()

    const error = new Error('error')

    saga.next()

    expect(saga.throw(error).value).toEqual(
      put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    )
  })

  test('should logOut', () => {
    const saga = logOut()

    expect(saga.next().value).toEqual(take(LOGOUT_REQUEST))

    expect(saga.next().value).toEqual(select(getAuth))

    const auth = {
      sessionId: '1234sd1124'
    }

    expect(saga.next(auth).value).toEqual(
      call(API.delete, 'authentication/session', {
        data: {
          session_id: auth.sessionId
        }
      })
    )

    expect(saga.next().value).toEqual(localStorage.removeItem('sessionId'))

    expect(saga.next().value).toEqual(localStorage.removeItem('user'))

    expect(saga.next().value).toEqual(
      put({
        type: LOGOUT_SUCCESS
      })
    )
  })

  test('should get error on log out', () => {
    const saga = logOut()

    saga.next()

    const error = new Error('error')

    expect(saga.throw(error).value).toEqual(
      put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    )
  })
})
