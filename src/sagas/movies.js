import API from '../services/api'
import { all, put, call, take, takeEvery, select } from 'redux-saga/effects'
import { getAuth } from '../selectors'
import {
  SEARCH_REQUEST,
  FETCH_SEARCH_MOVIES_REQUEST,
  FETCH_SEARCH_MOVIES_SUCCESS,
  FETCH_DISCOVER_MOVIES_REQUEST,
  FETCH_DISCOVER_MOVIES_SUCCESS,
  TOGGLE_SEARCH,
  FETCH_MOVIE_REQUEST,
  FETCH_MOVIE_SUCCESS,
  FETCH_MOVIE_CAST_SUCCESS,
  FETCH_MOVIE_CAST_REQUEST,
  FETCH_SIMILAR_MOVIES_REQUEST,
  FETCH_SIMILAR_MOVIES_SUCCESS,
  FETCH_PERSON_MOVIES_REQUEST,
  FETCH_PERSON_MOVIES_SUCCESS,
  FETCH_MOVIES_BY_GENRE_REQUEST,
  FETCH_MOVIES_BY_GENRE_SUCCESS,
  FETCH_MOVIE_STATUS_REQUEST,
  FETCH_MOVIE_STATUS_SUCCESS,
  SET_MOVIE_RATING_REQUEST,
  SET_MOVIE_RATING_SUCCESS,
  MARK_MOVIE_FAVORITE_REQUEST,
  MARK_MOVIE_FAVORITE_SUCCESS,
  FETCH_WATCHLIST_REQUEST,
  FETCH_WATCHLIST_SUCCESS,
  TOGGLE_MOVIE_WATCHLIST_REQUEST,
  TOGGLE_MOVIE_WATCHLIST_SUCCESS,
  SET_MESSAGE
} from '../constants'
import { reset } from 'redux-form'
import { push } from 'connected-react-router'

export function* searchSaga() {
  const { payload } = yield take(SEARCH_REQUEST)

  yield put({
    type: TOGGLE_SEARCH,
    payload: false
  })

  yield put(reset('search'))
  yield put(push(`/search/${payload}`))
}

export function* fetchSearchMovies() {
  while (true) {
    const { payload } = yield take(FETCH_SEARCH_MOVIES_REQUEST)

    try {
      const result = yield call(API.get, 'search/movie', {
        params: {
          query: payload.query,
          page: payload.page ? payload.page : 1
        }
      })

      yield put({
        type: FETCH_SEARCH_MOVIES_SUCCESS,
        payload: result.data
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export function* fetchDiscoverMovies() {
  while (true) {
    const { payload } = yield take(FETCH_DISCOVER_MOVIES_REQUEST)

    try {
      const result = yield call(API.get, `movie/${payload.type}`, {
        params: {
          page: payload.page ? payload.page : 1
        }
      })

      yield put({
        type: FETCH_DISCOVER_MOVIES_SUCCESS,
        payload: result.data
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export function* fetchSimilarMoviesSaga() {
  while (true) {
    try {
      const { payload } = yield take(FETCH_SIMILAR_MOVIES_REQUEST)

      const result = yield call(API, `/movie/${payload.id}/similar`, {
        params: {
          page: payload.page ? payload.page : 1
        }
      })

      yield put({
        type: FETCH_SIMILAR_MOVIES_SUCCESS,
        payload: {
          id: payload.id,
          data: result.data
        }
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export function* fetchMoviesByGenreSaga() {
  while (true) {
    try {
      const { payload } = yield take(FETCH_MOVIES_BY_GENRE_REQUEST)

      const result = yield call(API, `/discover/movie`, {
        params: {
          with_genres: payload.id,
          sort_by: payload.sort,
          page: payload.page ? payload.page : 1
        }
      })

      yield put({
        type: FETCH_MOVIES_BY_GENRE_SUCCESS,
        payload: result.data
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export function* fetchPersonMovies() {
  while (true) {
    try {
      const { payload } = yield take(FETCH_PERSON_MOVIES_REQUEST)

      const result = yield call(API, `/discover/movie`, {
        params: {
          with_cast: payload.id,
          page: payload.page ? payload.page : 1
        }
      })

      yield put({
        type: FETCH_PERSON_MOVIES_SUCCESS,
        payload: {
          id: payload.id,
          data: result.data
        }
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        error
      })
    }
  }
}

export function* fetchMovieSaga() {
  while (true) {
    const { payload } = yield take(FETCH_MOVIE_REQUEST)

    try {
      const result = yield call(API, `movie/${payload.id}`)

      yield put({
        type: FETCH_MOVIE_SUCCESS,
        payload: result.data
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        error
      })
    }
  }
}

export function* fetchWatchList() {
  while (true) {
    try {
      const { payload } = yield take(FETCH_WATCHLIST_REQUEST)

      const { sessionId } = yield select(getAuth)

      let path = ''

      switch (payload.type) {
        case 'watchlist':
          path = `account/${payload.accountId}/watchlist/movies`
          break

        case 'favorite':
          path = `account/${payload.accountId}/favorite/movies`
          break

        case 'rated':
          path = `account/${payload.accountId}/rated/movies`
          break

        default:
          break
      }

      const result = yield call(API, path, {
        params: {
          session_id: sessionId,
          sort_by: payload.sort,
          page: payload.page
        }
      })

      yield put({
        type: FETCH_WATCHLIST_SUCCESS,
        payload: result.data
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export function* fetchMovieStatus({ payload }) {
  try {
    const { sessionId } = yield select(getAuth)

    const result = yield call(API, `movie/${payload.id}/account_states`, {
      params: {
        session_id: sessionId
      }
    })

    yield put({
      type: FETCH_MOVIE_STATUS_SUCCESS,
      payload: result.data
    })
  } catch (error) {
    yield put({
      type: SET_MESSAGE,
      payload: {
        type: 'error'
      }
    })
  }
}

export function* setMovieRatingSaga() {
  while (true) {
    try {
      const { payload } = yield take(SET_MOVIE_RATING_REQUEST)
      const { sessionId } = yield select(getAuth)

      if (payload.type === 'set') {
        yield call(
          API.post,
          `movie/${payload.id}/rating`,
          {
            value: payload.value
          },
          {
            params: {
              session_id: sessionId
            }
          }
        )
      } else if (payload.type === 'delete') {
        yield call(API.delete, `movie/${payload.id}/rating`, {
          params: {
            session_id: sessionId
          }
        })
      }

      yield put({
        type: SET_MOVIE_RATING_SUCCESS,
        payload: payload.value
      })

      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'success',
          text: 'Your rating has been saved'
        }
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export function* markMovieFavoriteSaga() {
  while (true) {
    try {
      const { payload } = yield take(MARK_MOVIE_FAVORITE_REQUEST)
      const { user, sessionId } = yield select(getAuth)

      yield call(
        API.post,
        `account/${user.id}/favorite`,
        {
          media_type: 'movie',
          media_id: payload.id,
          favorite: payload.value
        },
        {
          params: {
            session_id: sessionId
          }
        }
      )

      yield put({
        type: MARK_MOVIE_FAVORITE_SUCCESS,
        payload: {
          value: payload.value,
          id: payload.id
        }
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

function* toggleWatchlistSaga() {
  while (true) {
    try {
      const { payload } = yield take(TOGGLE_MOVIE_WATCHLIST_REQUEST)
      const { user, sessionId } = yield select(getAuth)

      yield call(
        API.post,
        `account/${user.id}/watchlist`,
        {
          media_type: 'movie',
          media_id: payload.id,
          watchlist: payload.value
        },
        {
          params: {
            session_id: sessionId
          }
        }
      )

      yield put({
        type: TOGGLE_MOVIE_WATCHLIST_SUCCESS,
        payload: {
          value: payload.value,
          id: payload.id
        }
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export function* fetchCastSaga() {
  while (true) {
    const { payload } = yield take(FETCH_MOVIE_CAST_REQUEST)
    try {
      const result = yield call(API, `movie/${payload.movieId}/credits`)

      yield put({
        type: FETCH_MOVIE_CAST_SUCCESS,
        payload: { data: result.data, movieId: payload.movieId }
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export default function* root() {
  yield all([
    searchSaga(),
    fetchSearchMovies(),
    fetchMovieSaga(),
    fetchCastSaga(),
    fetchPersonMovies(),
    fetchDiscoverMovies(),
    fetchMoviesByGenreSaga(),
    fetchSimilarMoviesSaga(),
    takeEvery(FETCH_MOVIE_STATUS_REQUEST, fetchMovieStatus),
    setMovieRatingSaga(),
    markMovieFavoriteSaga(),
    fetchWatchList(),
    toggleWatchlistSaga()
  ])
}
