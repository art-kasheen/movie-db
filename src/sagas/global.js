import API from '../services/api'
import { all, put, call, takeEvery } from 'redux-saga/effects'
import {
  FETCH_CONFIG_SUCCESS,
  SIGN_IN_SUCCESS,
  APP_INIT_REQUET,
  APP_INIT_SUCCESS,
  FETCH_MOVIES_GENRES_SUCCESS,
  SET_MESSAGE
} from '../constants'

export function* init() {
  yield all([authInitSaga(), configurationSaga(), fetchMoviesGenresSaga()])

  yield put({
    type: APP_INIT_SUCCESS
  })
}

export function* authInitSaga() {
  const sessionId = yield localStorage.getItem('sessionId')
  const user = yield localStorage.getItem('user')

  if (sessionId && user) {
    yield put({
      type: SIGN_IN_SUCCESS,
      payload: {
        user: JSON.parse(user),
        sessionId
      }
    })
  }
}

export function* configurationSaga() {
  try {
    const result = yield call(API.get, 'configuration')

    yield put({
      type: FETCH_CONFIG_SUCCESS,
      payload: result.data
    })
  } catch (error) {
    yield put({
      type: SET_MESSAGE,
      payload: {
        type: 'error'
      }
    })
  }
}

export function* fetchMoviesGenresSaga() {
  try {
    const result = yield call(API.get, 'genre/movie/list')

    yield put({
      type: FETCH_MOVIES_GENRES_SUCCESS,
      payload: result.data
    })
  } catch (error) {
    yield put({
      type: SET_MESSAGE,
      payload: {
        type: 'error'
      }
    })
  }
}

export default function* root() {
  yield all([takeEvery(APP_INIT_REQUET, init)])
}
