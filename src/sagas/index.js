import { all } from 'redux-saga/effects'
import moviesSaga from './movies'
import globlaSaga from './global'
import personSaga from './person'
import adminSaga from './admin'

export default function* rootSaga() {
  yield all([adminSaga(), globlaSaga(), moviesSaga(), personSaga()])
}
