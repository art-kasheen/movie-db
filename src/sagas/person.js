import API from '../services/api'
import { all, put, call, take } from 'redux-saga/effects'
import {
  FETCH_PERSON_REQUEST,
  FETCH_PERSON_SUCCESS,
  SET_MESSAGE
} from '../constants'

export function* fetchPersonSaga() {
  while (true) {
    try {
      const { payload } = yield take(FETCH_PERSON_REQUEST)

      const result = yield call(API, `person/${payload}`)

      yield put({
        type: FETCH_PERSON_SUCCESS,
        payload: result.data
      })
    } catch (error) {
      yield put({
        type: SET_MESSAGE,
        payload: {
          type: 'error'
        }
      })
    }
  }
}

export default function* root() {
  yield all([fetchPersonSaga()])
}
