import {
  searchSaga,
  fetchSearchMovies,
  fetchDiscoverMovies,
  fetchMoviesByGenreSaga,
  fetchPersonMovies,
  fetchMovieSaga,
  fetchWatchList,
  setMovieRatingSaga,
  markMovieFavoriteSaga,
  fetchCastSaga,
  fetchMovieStatus
} from './movies'
import { take, put, call, select } from 'redux-saga/effects'
import { getAuth } from '../selectors'
import {
  SEARCH_REQUEST,
  TOGGLE_SEARCH,
  FETCH_SEARCH_MOVIES_REQUEST,
  FETCH_SEARCH_MOVIES_SUCCESS,
  FETCH_DISCOVER_MOVIES_REQUEST,
  FETCH_DISCOVER_MOVIES_SUCCESS,
  FETCH_MOVIES_BY_GENRE_REQUEST,
  FETCH_MOVIES_BY_GENRE_SUCCESS,
  FETCH_PERSON_MOVIES_REQUEST,
  FETCH_PERSON_MOVIES_SUCCESS,
  FETCH_MOVIE_REQUEST,
  FETCH_MOVIE_SUCCESS,
  FETCH_WATCHLIST_REQUEST,
  FETCH_WATCHLIST_SUCCESS,
  SET_MOVIE_RATING_REQUEST,
  SET_MOVIE_RATING_SUCCESS,
  SET_MESSAGE,
  MARK_MOVIE_FAVORITE_REQUEST,
  MARK_MOVIE_FAVORITE_SUCCESS,
  FETCH_MOVIE_CAST_REQUEST,
  FETCH_MOVIE_CAST_SUCCESS,
  FETCH_MOVIE_STATUS_REQUEST,
  FETCH_MOVIE_STATUS_SUCCESS
} from '../constants'
import API from '../services/api'
import { reset } from 'redux-form'
import { push } from 'connected-react-router'
import { movies } from '../mocks/movies'

describe('Movies Saga', () => {
  test('should toggle search', () => {
    const saga = searchSaga()

    const action = {
      type: SEARCH_REQUEST,
      payload: 'query'
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(
      put({
        type: TOGGLE_SEARCH,
        payload: false
      })
    )

    expect(saga.next().value).toEqual(put(reset('search')))

    expect(saga.next().value).toEqual(put(push(`/search/${action.payload}`)))
  })

  test('should fetch search movies', () => {
    const saga = fetchSearchMovies()

    const action = {
      type: FETCH_SEARCH_MOVIES_REQUEST,
      payload: {
        query: 'query',
        page: '1'
      }
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(
      call(API.get, 'search/movie', {
        params: {
          query: action.payload.query,
          page: action.payload.page ? action.payload.page : 1
        }
      })
    )

    const result = {
      data: movies
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_SEARCH_MOVIES_SUCCESS,
        payload: result.data
      })
    )
  })

  test('should fetch discover movies', () => {
    const saga = fetchDiscoverMovies()

    const action = {
      type: FETCH_DISCOVER_MOVIES_REQUEST,
      payload: {
        type: 'type',
        page: '1'
      }
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(
      call(API.get, `movie/${action.payload.type}`, {
        params: {
          page: action.payload.page ? action.payload.page : 1
        }
      })
    )

    const result = {
      data: movies
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_DISCOVER_MOVIES_SUCCESS,
        payload: result.data
      })
    )
  })

  test('should fetch movies by genre', () => {
    const saga = fetchMoviesByGenreSaga()

    const action = {
      type: FETCH_MOVIES_BY_GENRE_REQUEST,
      payload: {
        id: 1,
        sort: 'pupularity',
        page: '1'
      }
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(
      call(API, `/discover/movie`, {
        params: {
          with_genres: action.payload.id,
          sort_by: action.payload.sort,
          page: action.payload.page ? action.payload.page : 1
        }
      })
    )

    const result = {
      data: movies
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_MOVIES_BY_GENRE_SUCCESS,
        payload: result.data
      })
    )
  })

  test('should fetch person movies', () => {
    const saga = fetchPersonMovies()

    const action = {
      type: FETCH_PERSON_MOVIES_REQUEST,
      payload: {
        id: '1',
        page: '1'
      }
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(
      call(API, `/discover/movie`, {
        params: {
          with_cast: action.payload.id,
          page: action.payload.page ? action.payload.page : 1
        }
      })
    )

    const result = {
      data: movies
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_PERSON_MOVIES_SUCCESS,
        payload: {
          id: action.payload.id,
          data: result.data
        }
      })
    )
  })

  test('should fetch movie', () => {
    const saga = fetchMovieSaga()

    const action = {
      type: FETCH_MOVIE_REQUEST,
      payload: {
        id: '1'
      }
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(
      call(API, `movie/${action.payload.id}`)
    )

    const result = {
      data: movies[0]
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_MOVIE_SUCCESS,
        payload: result.data
      })
    )
  })

  test('should fetch movie status', () => {
    const action = {
      type: FETCH_MOVIE_STATUS_REQUEST,
      payload: {}
    }

    const saga = fetchMovieStatus(action)

    expect(saga.next().value).toEqual(select(getAuth))

    const auth = {
      user: { name: 'name' },
      sessionId: 'dsfjksdf1231,mm21lqsd-'
    }

    expect(saga.next(auth).value).toEqual(
      call(API, `movie/${action.payload.id}/account_states`, {
        params: {
          session_id: auth.sessionId
        }
      })
    )

    const result = {
      data: movies
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_MOVIE_STATUS_SUCCESS,
        payload: result.data
      })
    )
  })

  test('should fetch watchlist', () => {
    const saga = fetchWatchList()

    const action = {
      type: FETCH_WATCHLIST_REQUEST,
      payload: {
        type: 'watchlist'
      }
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(select(getAuth))

    let path = ''

    switch (action.payload.type) {
      case 'watchlist':
        path = `account/${action.payload.accountId}/watchlist/movies`
        break

      case 'favorite':
        path = `account/${action.payload.accountId}/favorite/movies`
        break

      case 'rated':
        path = `account/${action.payload.accountId}/rated/movies`
        break

      default:
        break
    }

    const auth = {
      user: { name: 'name' },
      sessionId: 'dsfjksdf1231,mm21lqsd-'
    }

    expect(saga.next(auth).value).toEqual(
      call(API, path, {
        params: {
          session_id: auth.sessionId,
          sort_by: action.payload.sort,
          page: action.payload.page
        }
      })
    )

    const result = {
      data: movies
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_WATCHLIST_SUCCESS,
        payload: result.data
      })
    )
  })

  test('should set movie rating', () => {
    const saga = setMovieRatingSaga()

    const action = {
      type: SET_MOVIE_RATING_REQUEST,
      payload: {
        id: '1',
        value: true,
        type: 'set'
      }
    }

    const auth = {
      user: { name: 'name' },
      sessionId: 'dsfjksdf1231,mm21lqsd-'
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(select(getAuth))

    expect(saga.next(auth).value).toEqual(
      call(
        API.post,
        `movie/${action.payload.id}/rating`,
        {
          value: action.payload.value
        },
        {
          params: {
            session_id: auth.sessionId
          }
        }
      )
    )

    expect(saga.next().value).toEqual(
      put({
        type: SET_MOVIE_RATING_SUCCESS,
        payload: action.payload.value
      })
    )

    expect(saga.next().value).toEqual(
      put({
        type: SET_MESSAGE,
        payload: {
          type: 'success',
          text: 'Your rating has been saved'
        }
      })
    )
  })

  test('should mark movie as favorite', () => {
    const saga = markMovieFavoriteSaga()

    const action = {
      type: MARK_MOVIE_FAVORITE_REQUEST,
      payload: {}
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(select(getAuth))

    const auth = {
      user: { name: 'name' },
      sessionId: 'dsfjksdf1231,mm21lqsd-'
    }

    expect(saga.next(auth).value).toEqual(
      call(
        API.post,
        `account/${auth.user.id}/favorite`,
        {
          media_type: 'movie',
          media_id: action.payload.id,
          favorite: action.payload.value
        },
        {
          params: {
            session_id: auth.sessionId
          }
        }
      )
    )

    expect(saga.next().value).toEqual(
      put({
        type: MARK_MOVIE_FAVORITE_SUCCESS,
        payload: {
          value: action.payload.value,
          id: action.payload.id
        }
      })
    )
  })

  test('should fetch cmovie cast', () => {
    const saga = fetchCastSaga()

    const action = {
      type: FETCH_MOVIE_CAST_REQUEST,
      payload: {}
    }

    expect(saga.next().value).toEqual(take(action.type))

    expect(saga.next(action).value).toEqual(
      call(API, `movie/${action.payload.movieId}/credits`)
    )

    const result = {
      data: movies
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_MOVIE_CAST_SUCCESS,
        payload: { data: result.data, movieId: action.payload.movieId }
      })
    )
  })
})
