import {
  fetchMoviesGenresSaga,
  configurationSaga,
  authInitSaga,
  init
} from './global'
import API from '../services/api'
import { all, call, put } from 'redux-saga/effects'
import {
  FETCH_MOVIES_GENRES_SUCCESS,
  FETCH_CONFIG_SUCCESS,
  SIGN_IN_SUCCESS,
  APP_INIT_SUCCESS
} from '../constants'
import { movies } from '../mocks/movies'

describe('Global Saga', () => {
  test('should initialize app', () => {
    const saga = init()

    expect(saga.next().value).toEqual(
      all([authInitSaga(), configurationSaga(), fetchMoviesGenresSaga()])
    )

    expect(saga.next().value).toEqual(
      put({
        type: APP_INIT_SUCCESS
      })
    )
  })

  test('should auth init app', () => {
    const saga = authInitSaga()

    const user =
      '{"avatar":{"gravatar":{"hash":"sd323dsda1eaeewe2311wqs"}},"id":1,"iso_639_1":"en","iso_3166_1":"US","name":"","include_adult":false,"username":"name"}'

    const sessionId = '12jjrdj45js'

    expect(saga.next().value).toEqual(localStorage.getItem('sessionId'))

    expect(saga.next(sessionId).value).toEqual(localStorage.getItem('user'))

    expect(saga.next(user).value).toEqual(
      put({
        type: SIGN_IN_SUCCESS,
        payload: {
          user: JSON.parse(user),
          sessionId
        }
      })
    )
  })

  test('should fetch configuration', () => {
    const saga = configurationSaga()

    expect(saga.next().value).toEqual(call(API.get, 'configuration'))

    const result = {
      data: 'config'
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_CONFIG_SUCCESS,
        payload: result.data
      })
    )
  })

  test('should fetch movies genres', () => {
    const saga = fetchMoviesGenresSaga()

    expect(saga.next().value).toEqual(call(API.get, 'genre/movie/list'))

    const result = {
      data: movies
    }

    expect(saga.next(result).value).toEqual(
      put({
        type: FETCH_MOVIES_GENRES_SUCCESS,
        payload: result.data
      })
    )
  })
})
